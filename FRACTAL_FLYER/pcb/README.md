# PCB Design

The hardware for the fractal flyers consists mainly of the head board and tail board. The design files for these boards are located in this directory.

The tailboard has several versions to make versioning easier without complex git commands and to show the progression of the board. The versions are:

- tailboard: the first version, completed by students in EE285 in the winter of 2020.
- tailboard-v2: changes accelerometers to use 3V instead of 5V
- tailboard-v3: fixes pinout of Feather M4 board
- tailboard-v4: changes two wing accelerometers to I2C from SPI, further
  fixes to pinout of Feather M4 board.

# Reference

* [KiCad](https://kicad-pcb.org/)
* [SAM32 Board Files](https://github.com/maholli/SAM32/tree/master/hardware/SAM32_v26)
* [TI WEBENCH Power Designer](https://www.ti.com/design-resources/design-tools-simulation/webench-power-designer.html)
* [Upverter](https://upverter.com/)
