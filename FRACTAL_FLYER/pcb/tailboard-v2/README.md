# Fractal Flyer Tailboard PCB V2

The tailboard is the board on each Fractal Flyer. The headboard is the
board at the other end of the Ethernet cable, connected to many USB ports 
on the controller computer in parallel and 48V power supplies,

The BoM of the tailboard can be found here: <https://www.digikey.com/short/z8m375>

Version 2 of the the tailboard changes the pin orders of the JST XH
connectors for the accelerometers and LED strips to match the accelerometer
boards and LED strip connectors. It does so that a 
flat cable can connect the board to them; with the V1 orderings,
pins needed to be reordered.
