# Course Materials for EE185/EE285/CS241

This directory stores materials for the course offerings
of EE185/EE285/CS241. These courses have been offered five times:

  - autumn-19 (in person): designing FLIGHT and making an initial prototype of a Fractal Flyer.
  - winter-20 (in person, truncated): making a first version of a Fractal Flyer by developing the body shell, wings, wing mechanism, circuit board on each Flyer, and the LED cross within the body.
  - spring-20 (remote): designing and writing the FlightGUI graphical programming tool as well as systems infrastructure for a detailed graphical simulator.w
  - winter-21 (remote): Fractal Flyer firmware, finishing FlightGUI for programming the installation and the graphical components of the simulator; [materials are stored as Issues in GitLab](https://code.stanford.edu/plevis/ee185/-/issues?scope=all&state=closed&search=Winter+2021).
  - autumn-21 (in person): the wings, the body shell, the circuit board on each Flyer, the digital signaling to the Flyer board, and the firmware image running on each Flyer.
  - spring-22 (in person): completing wing construction, completing and manufacturing the body shell, and designing the mounting mechanisms 

