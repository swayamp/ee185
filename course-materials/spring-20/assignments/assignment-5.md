# Assignment 5: Building Out the Framework

*Written by Philip Levis*

**Due: Tuesday, May 26, 2020 at 2:30 PM (before class)**

## Goals

When you're done with this assignment, you should
  - have a complete end-to-end system that reads in the state of FLIGHT in the UI and sends that to real Fractal Flyers or the simulator and a test that checks it's working,
  - have a simulator that safely and correctly cleans up when it exits,
  - have two prototype Effect implementations that use the Java data model,
  - be able to save and restore playlists of effects,
  - be able to place and configure Fractal Flyers in a Processing-based UI,
  - have an imported model of the stairwell in OpenGL,
  - have documented the software architecture and major APIs.

I've attached names to each task. This means that you are responsible
for that task being completed. It doesn't mean you have to do it all
by yourself -- if it's much harder than it seems, feel free to ask for
help from others. Or, if you want, swap work around. But you'll be
responsible for, next class, reporting out on completion and questions
that came up.

## Playlists (Moses and Martin)

Now that we have a completed data model, we can start running Effects that
modify the data model. Furthermore, we can store playlists of these Effects
and parameters, such that we have repeatable animations.

Look at how SQUARED stores and restores playlists, then runs them. Add
a run-time (command-line) option to the FLIGHT UI that loads a
playlist from a file and runs it. Your goal is to write two simple
playlists, which you can load and run for different animations. These
playlists can be simple, e.g., just alternating between two different
Effects.

This part of LXStudio is quite complex -- I sent email to Mark and
cc'd you to ask for some help. You do not need to be able to record
playlists.  The goal is that you can load and run a simple playlist.

This will involve some coordination with the Effect group, as you will
need to be able to access the loaded Effects. You will need to agree
on how this is stored.

## Simulator Execution Management (Andrew)

Now that you can start up the Python processes and the main simulator
process, you should make this startup rock-solid (no "this sleep is
long enough") and incorporate proper cleanup. The simulator should run
until it is explicitly shut down. E.g., one can attach a UI, kill the
UI, and re-attach the UI. The system:

  - Should shut down interpreters and the file system when the simulator
  exits (or crashes).
  - Should be able to shut down/restart interpreters that are stuck in
  infinite loops.
  - Should restart a interpreter if it crashes (e.g., due to a bug in
  the C/Python library).
 
## OpenGL Model and Initial Render (Samsara)

The simulator should be able to take the DWG file for the stairwell
and produce an OpenGL render of it, with the back wall white, the
front glass as glass (or clear) and everything else as grey.

The simulator should also be able to read in the configuration file of
the Fractal Flyers (including their positions) and draw Fractal Flyers
in these positions. Simple cones or tetrahedra are fine for now.

## Effects (Ashley)

Complete two effects as described in [Assignment 4](assignment-4.md).

## FF Configuration File (Ashley w/Samsara)

Finalize the data model for the Fractal Flyers, particularly their
metadata and how their location is specified. Read this file into
the UI and translate the logical coordinates into X, Y, Z that can be
then accessed by the UI visualization.

## Refine Documentation (Everyone)

Starting with your initial notes from last week, flesh out the
documentation for your software architecture. Use [this piece of
documentation from the Tock Operating
System](https://www.github.com/tock/tock/blob/master/doc/Overview.md)
as a model for how to describe your software. You should:
  - Clearly state what this directory contains
  - Describe how it interacts with the other major programs
  - Have a figure that shows its overall structure and architecture
  - Gives an overview of the major subcomponents, saying what they conceptually do

Your model should be that you are writing to someone who has never
looked at your code, but wants to know what it does and where, if they
need to add a feature or find a bug, where they should look first.


