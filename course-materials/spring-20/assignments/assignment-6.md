# Assignment 5: Building Out the Framework

*Written by Philip Levis*

**Due: Tuesday, June 2, 2020 at 2:30 PM (before class)**

## Goals

When you're done with this assignment, you should
  - have a complete end-to-end GUI that allows you to mix several patterns, move Flyers, and generate Python scripts that make the simulator (or installation) reflect the GUI,
  - finish the simulator control plane,
  - have a data model that allows implementation and mixing of Patterns,
  - be able to save and restore playlists of multiple Patterns, and
  - have a GUI visualization of the stairwell and flyers in OpenGL.

I've attached names to each task. This means that you are responsible
for that task being completed. It doesn't mean you have to do it all
by yourself -- if it's much harder than it seems, feel free to ask for
help from others. Or, if you want, swap work around. But you'll be
responsible for, next class, reporting out on completion and questions
that came up.

## End-to-end Test (Phil, Samsara, and Martin)

This involves mixing Patterns in the GUI and checking that they then appear
in the simulation visualization.

## Simulator control plane (Andrew)

We noted several things to polish in the simulator control plane:
  1. Have a mechanism for restarting problematic Flyer Python processes,
  2. Properly cleaning up on simulator crash and kill (note you cannot catch SIGKILL), and
  3. (in the MR) Improving the UI/console interface to the simulator.

## Data Model for Patterns (Ashley)

As discussed in class, the FlightModel should have LXPoints, such that LXStudio
can access and understand them. This is necessary so that Patterns will each get
a copy of the color state they can operate on. Note that this by default appears
as an `int[]` of the color array. I'd suggest subclassing `Pattern`
so that you get something like the current `FlightModel` on top of the underlying
`color` array.
This means that all Patterns will subclass this `FlightPattern`
and be able to easily access the replicated state in a pattern.

As part of this, implement two Patterns to check that they each individually
set their own color state, which can then be mixed.

## Playlists (Moses and Martin)

Now that we have working playlists, incorporate playlist mixing into Engine.
You can use SQUARED as a template. Note that each Pattern should be able to
specify how it is mixed: ADD, MUL, SUB, etc. Be sure to use LXParameters, so
that this can be easily mixed through a GUI.

Verify that we can store playlists of multiple patterns.

## Simulator GUI (Samsara)

The simulator should be able to take the DWG file for the stairwell
and produce an OpenGL render of it, with the back wall white, the
front glass as glass (or clear) and everything else as grey.

The simulator should also be able to read in the configuration file of
the Fractal Flyers (including their positions) and draw Fractal Flyers
in these positions. Simple cones or tetrahedra are fine for now.



