# Assignment 2: Making a Plan 

*Written by Philip Levis*

**Due: Sunday, April 3, 2022 at 6:00 PM**

This quarter, you're each going to work in a group to
complete an important piece of FLIGHT. When embarking on
an engineering project such as this, you want to plan.
What's going to take a lot of time? What can you work
on in parallel? How are you going to complete your task
on time without pulling lots of all-nighters?

But even before you get to making a plan, you want to
talk about *how* you will work together and come up
with shared goals. You don't want to just be a group.
You want to be *team*.

## Goals
When you're done with this assignment, you should have

- grown from a group into a team, with shared goals
and expectations;

- read the core design materials for a FLIGHT and
a Fractal Flyer, understanding both your part and
how the whole system comes together;

- have written a plan and timeline for your project
over the next 7 weeks.

## 1 Form a Team
*cribbed from Erin MacDonald*

The most important parts of a successful team are:

  1. Finding a time when everyone meets, as a group, each week, for at least two hours.
  2. Having similar expectations for the course and the amount of time you will spend on it. Each student should expect to spend about nine hours outside of class per week on FLIGHT.
  3. Having a mix of skills. 

We've tried to help with #3. The first part of this assignment is about #1 and #2.

### Find a Time to Meet

Compare your schedules to find a time block of at least two hours when you all
can meet. Commit to meeting at this time each week and working together. If 
something comes up and someone can't make it, be sure to schedule another time
for that week.

### Write a Charter

A team charter is a document of your own design. It should be "artfully" designed, 
expressing some interests and passions of your team. Things to discuss:
  1. What are your goals for the class? Please have each member discuss individually.
  2. Talk about some triumphs and challenges on past team assignments.
  4. Take this [quiz](https://whatkindofdesigner.com/) Discuss whether or not you agree with the classification. It will give you a starting point for discussing your interests in the design process.

Include in the charter:

  1. What is your team mascot? 
  2. How will the team celebrate triumphs?
  3. How will the team make important decisions? 
  4. How will the team resolve conflicts and discuss problems?
  5. Leadership: What does "leadership" mean to your team? Who is the team leader to start with? How often or under what circumstances will you switch leaders? (Recommended that you switch at least 1 time and no more than 2 times. I think it is better to choose a date in advance, that way there are no hard feelings when you switch.)
  6. Who is the person that hits "submit" on your reports and milestones? The one who crosses the "t"s and dots the "i"s? This is a big job, possibly more work than the leader, so plan accordingly. You can assign different people to be in charge of different reports and milestones, but it helps to have a clear person in charge for each one.
  7. What are the skills of the team members? What special skill does everyone bring to the team?
  8. When will the team meet as a group each week? Please be precise.
  9. What will be the procedure for missing or being late to this meeting? How much advance notice must be given and using what method? (We recommend that last-minute text messaging is not used to inform team members that a member will be late or miss a meeting.) 

One note on leadership: I encourage you strongly to have a team
leader. In the past, some groups have shied away from having a leader,
choosing consensus or democractic voting to make decisions. In my
experience, teams adopting this approach have struggled to complete
their goals. A group leader can keep the group on track, keep the
group working smoothly together, be a spokesperson to the course
staff, and provide a second pair of eyes and ideas when problems come
up.


## 2 Review Existing Materials

The course repository has a lot of materials on FLIGHT and
Fractal Flyers.  Understanding your project, and how it fits
into the larger whole, is an important first step to being
able to complete the project. If you don't know what you
have to do, then it's very hard to do it! This is especially
important in engineering a complex system, where there are
many dependencies and constraints from all of the different
parts. 

You've already looked at the repository and read the design document
in the first week of class. There's a lot more, detailed information
for your project:

  - If you are in the **body shell** group, read the two body shell
  documents, from winter of 2020 and autumn of 2021.
  - If you are in the **wings** group, read the winter 2020 reports
  on wing materials, wing production decisions, and hinge mechanism
  as well as the wings report from 2021.
  - If you are in the **mountings** group, spend 20 minutes in the
  Packard stairwell, looking at its structure and how flyers might
  be atached. Watch Alex Slocum's 2.75 videos for Topic 8 (Structures):
     o [Topic 8.1](https://youtu.be/xNDWliDootk)
	 o [Topic 8.2](https://youtu.be/6zUgE6ZjvWo)
	 o [Topic 8.3](https://youtu.be/nqC8Cf2DKY4)
  These lectures are from a famous class at MIT (2.007) where students competitively
  build robots to complete a task. After you've watched these videos, 
  review the example attachment mechanisms that Charlie has found.
  

## 3 Planning (the next seven weeks of) Your Future

Now that you've talked, have shared your expectations, 
have thought about how you will work as a team, and understand
your project, it's time to do your first joint task: making a 
plan and schedule.

This plan is your roadmap for the next seven weeks. You're going
to think about exactly what you have to achieve and how you're
going to do it. You want to take a large, multifaceted project
and break it up into smaller, more predictable, manageable tasks.

This roadmap is not sarosanct: we expect it will change and
adjust as we talk you through it and as issues come up. But when
things do change, having thought a plan through will let you
more easily understand the implications and how you want to adapt
in response.

The last 2 weeks of the quarter will be building
complete Fractal Flyers and getting them working. We're setting
asidse two weeks because it is almost certain that your schedules
will slip a little, due to details or unforeseen challenges (e.g.,
a package containing materials is delayed a few days).
You're therefore making a plan for weeks 2-8. The wings
group may schedule workshops in weeks 9 and 10, but they
should be final workshops to finish up: the expectation
is that by the end of week 8 the building process is extremely
polished and working with high yield.

Your plan should:

  - Complete the tasks laid out in your one-page project description.
  - Have a week-by-week statement of expected progress. Some
  tasks will take more than 1 week. In this case, try breaking it
  up into multiple tasks. Come up with smaller steps that you can
  test and check. 
  - Clearly delineate tasks that occur in parallel. For example,
  if half of your group is going to research connectors and half
  is going to make a mould, show these as two separate tasks 
  occurring in parallel.
  - Have at least 2 major milestones. These should be significant
  pieces of work which, when completed, represent a triumph or
  large accomplishment.
  - Include written reports that explain designs and important
  design decisions. It is better to write a small number of
  smaller reports on separate topics than trying to write one
  big report at the end. These reports serve two audiences:
  the current class, so you can explain and justify your
  decisions, and future contributors to FLIGHT, so they can
  learn what you know and carry that knowledge forward.
  - State which aspects of your plan and estimates have the 
  greatest and least certainty. Uncertainty can be because 
  you don't know, or because there are factors outside your
  control. 
  - Include risk mitigation: if something goes wrong, what is
  your plan B? What will you fall back to? It could be that the 
  fallback is less attractive, costs more, or has other limitations.
  Thinking through other approaches is a key part of coming up
  with a good approach: otherwise, it's too easy to tunnel.
  - Write down any expected dependencies on other groups. On what
  will you need to coordinate, and when?
  - Are there big unknowns, tools you need, or expertise your are missing? If 
  you could have an advisor or resource of some kind (e.g., an
  expert on the thermal and physical propeties of acrylic,
  or a vector network analyzer), tell us what it is so we
  can help.

Your plan should be 2-3 pages. You can use a spreadsheet to 
show timelines, or figures, or tables. Whatever works best for you.

If you have questions or are uncertain about certain aspects of
the Flyer, do not hesitate to reach out. You can email all of us
at the staff mailing list ee185-aut2122-staff@lists.stanford.edu 
(since the staff is the same as autumn we're just reusing the
list).

We will share and discuss the plans in class on Monday.

## 4 Handing in

Please send an email to the staff list with subject "Assignment 2"
which has your your charter and plan.  Please send both as attachments
(PDF if possible). Do not send links to Google docs, or Drive entries,
or Dropbox files. If you spoke with people outside the class in coming
up with your plan (e.g., on certain technical topics), great! Please
mention them in your report.

