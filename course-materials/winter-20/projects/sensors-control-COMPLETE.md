# Project: Wing Sensors, Actuation, and Control Loop

*Instructor Lead: Philip Levisn*

**Due: Tuesday, January 21, 2020**

The goals and deliverables for this project are:
  1. Determine which sensors and circuits will be used to control wing motion,
  2. Determine how the sensors will be attached to the mechanism,
  3. Build and test the sensors and circuits, 
  4. Write stable control software for the system, and
  5. Demonstrate and test the control software.

## Sensors and Circuits

Each shape has two wings, which are individually controllable from 
a CortexM board runnig CircuitPython. Decide how the board will sense
the current position of the wing and how it will control the DC motor
to change position. Last quarter, the conclusion was to use a rotary
encoder for sensing (the board has a quadrature decoding peripheral) and 
an H-bridge for the motor (controlled via PWM). If you choose to diverge
from this approach, clearly articulate why. Choose and order specific parts for
your approach.

## Sensing attachment

Coordinate with the mechanism team to determine where the position sensor
will go. It is important that the sensor measures the actual position of
the wing, not its driving shafts: otherwise slippage in the gears will lead
to errors.

## Build and test the sensors and circuits

Build and wire up the full circuits for your sensing and actuation.
Test them with a Maxwell or Feather board (the SAM32D MCU we are using).
Verify that you can accurately sense the wing position and that you can
precisely control the motion of the wing.

## Write stable control software for the system

Decide on a control system to use (e.g., proportional–integral–derivative/PID).
Implement it such that you can specify a position to move the wing to and
it moves smoothly, with the wing velocity being independent of its position.
I.e., it should not move slower when horizontal and faster when vertical,
but its velocity curve may be smooth such that it has a start-up acceleration
and final slow-down.

## Demonstrate and test the control software

Demonstrate that your circuits and supporting software can stably control
the position of the wing. Collaborate with the mechanism team to have something
to test and demontrate with.


