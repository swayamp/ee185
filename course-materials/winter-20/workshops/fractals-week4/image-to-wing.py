#!/usr/bin/env python3

import cv2
import numpy as np
import sys

if len(sys.argv) != 2:
  print("usage: ", sys.argv[0], "FILE")
  sys.exit(1)

input = sys.argv[1]
print("Reading", input)
image = cv2.imread(input, cv2.IMREAD_COLOR)
(iheight, iwidth, idepth) = image.shape

tokens = input.split(".")
print(tokens)
leading = tokens[0:-1]
type = tokens[-1]

filename = ".".join(leading)
cropped_file = filename + "-cropped." + type

print("Will produce output", cropped_file)
h = 501
w1 = 1375
w2 = 1659
aspect_ratio = float(h) / float(w2)

oheight = 0
owidth = 0
odepth = idepth
if aspect_ratio * iwidth > iheight:
  oheight = int(iheight)
  owidth = int(iheight / aspect_ratio)
else:
  owidth = int(iwidth)
  oheight = int(owidth * aspect_ratio)

wscale = owidth / iwidth
hscale = oheight / iheight

mid = float(w1) / float(w2)
midpoint = int(mid * owidth)

print("Source is " + str(iwidth) + "x" + str(iheight))
print("Output is " + str(owidth) + "x" + str(oheight))
print("Width scale is " + str(wscale) + ", height scale is " + str(hscale));
print("Midpoint is at", midpoint)

output = np.ndarray([oheight, owidth, odepth])
output.fill(0) # Fill it black

threshold = 750 

def sample(image, y, x):
  [r, g, b] = image[y, x]
  val = int(r) + int(g) + int(b)
  if val > threshold:
    return [255, 255, 255]
  for dx in range(-1, 2):
    for dy in range(-1, 2):
      newx = x + dx
      newy = y + dy
      if newx < 0 or newx >= iwidth or newy < 0 or newy >= iheight: 
        return [255, 255, 255]
      [newr, newg, newb] = image[newy, newx]
      newval = int(newr) + int(newg) + int(newb)
      if newval >= threshold:
        return [0, 0, 0]
  return [255, 255, 255]

# Make the cropped output

output = np.ndarray([oheight, owidth, odepth])
output.fill(0) # Fill it black

for x in range(midpoint):
  fraction = float(x) / float(midpoint)
  stop = (fraction * oheight) 
  for y in range(oheight):
    if y < int(stop): 
      output[y, x] = sample(image, y, x)

for x in range(midpoint, owidth):
  fraction = 1.0 - (float(x - midpoint) / float(owidth - midpoint))
  stop = fraction * oheight
  for y in range(oheight):
    if y < int(stop):
      output[y, x] = sample(image, y, x)

cv2.imwrite(cropped_file, output)

