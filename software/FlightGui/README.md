# FLIGHT GUI

This directory contains the Processing UI for FLIGHT, the light art
installation in Stanford's Packard building. To run the GUI,

1. download [Processing](https://processing.org/download),
2. compile the Java UI and make its jar `flight.jar` by running `./compile` in `ui/`,
3. run Processing and load `FlightGui.pde` as your sketch.

![FlightGui UI](flightgui.png)

You can position where Fractal Flyers are placed with the Layout Tool
on the left, mix animation patterns with the mixer tool on the bottom,
assign which pattern is on which channel with the pattern selection tool
on the right, and save/playback particular mixes with the track tool
on the bottom right.

Wing animation is controlled by whatever pattern is in channel 1.

## Directory structure and files

Processing
requires that a file named X.pde is in a directory named X, so all of
the Processing code needs to be in a separate directory if you want
your libraries to follow standard Java package structure.

`FlightGui.pde` is the top-level Processing sketch to start the
GUI. `UIComponents.pde` contains the library of GUI
elements. `Drawing.pde` contains the routines that draw FLIGHT (the
Packard stairwell, the Fractal Flyers) in the stairwell.
This screensho shows the UI with the corresponding Java class names from
`UIComponents.pde`.

Processing requires that all external libraries be in the `code`
subdirectory. This directory has symlinks to all of the libraries it
needs.

The `data` subdirectory is a symlink to `ui/data`, as this is where
the UI keeps its data files, such as the JSON file describing the
layout and metadata of Fractal Flyers.
