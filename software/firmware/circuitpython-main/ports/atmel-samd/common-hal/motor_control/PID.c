#include "ports/atmel-samd/common-hal/motor_control/PID.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "peripherals/samd/pins.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

// PWM control constants
#define ERR_THRESHOLD_DEGREES 2
#define BEGIN_SLOW_DEGREES 5
#define MAX_DUTY  0.9 //Set to PWM %high that represents max speed we want the wings to move
#define MIN_DUTY  0.0
#define UP true
#define DOWN false
#define LEFT true
#define RIGHT false

// Error case definitions
#define CALC_ERROR_NUM (float) -5000
#define ERROR_HALT (float) 5000.0
#define RESET_NUM_CENTER -1000.0
#define RESET_NUM_WING (float) 1000.0 // this error should pause the wings + reset motors 

#define EPSILON 0.005 // The threshold in which two floats are conisdered equal

// PID controller constants
// TODO: add approprioate timing for cycle time
#define CYCLE_TIME 0.02 // The amount of time between interrupts in s 
#define BUFFER_SIZE 50
#define KI 0.0475 // PID Integral Component gain
#define KP 0.0025 // PID Position Component gain
static float prior_integral = 0; // TODO: maintain bounds on this integral so it doesn't loop around, and figure out when to reset to 0!!
static float error_buffer[BUFFER_SIZE]; // should init to zeros
static int oldest_elem_index = 0; 

extern void set_pwm(bool left, bool up, float duty);

/*The following is based on the EE285 control loop python code (see the repository for details)*/

acceleration get_bird_accel(void) {
  return get_raw_accel(CENTER);
}

bool float_equality(float a, float b) {
  return fabs((double)(a - b)) < (double)EPSILON;
}

// accepts an acceleration value
// returns true if value is between 9 and 11 m/s^2
// returns false in all other cases
bool valid_magnitude(acceleration accel){
    float mag = pow(accel.x,2)+pow(accel.y,2)+pow(accel.z,2);
    return (mag > 81 && mag < 121);
}

float pid_update_pwm(float current_angle, float degrees_C){
    float diff = abs(current_angle - degrees_C);
    float duty = 0;

    // Integral Component of PID Calculations
    
    // Get rid of the oldest error and put in the newest error
    float integral = prior_integral + (diff * CYCLE_TIME) - (error_buffer[oldest_elem_index] * CYCLE_TIME);
    prior_integral = integral;

    // Update the buffer with the newest error value
    error_buffer[oldest_elem_index] = diff;
    oldest_elem_index += 1;
    if (oldest_elem_index > BUFFER_SIZE) {
      oldest_elem_index = 0;
    }
    
    // Integrate P and I components of PID
    float tempduty = (KP * diff) + (KI * integral);
    printf("Proposed pre-clamped duty cycle: %.5f given calculated diff: %.5f, and calculated integral: %.5f\n", (double)tempduty, (double) diff, (double) integral);

    // Bound the output of the controller to our chosen min + max duty cycles
    if ( tempduty > MAX_DUTY){
      duty = MAX_DUTY;
    }
    else if (tempduty < MIN_DUTY){
      duty = MIN_DUTY;
    }
    else{
      duty = tempduty;
    }
    printf("Proposed duty cycle: %.5f given calculated diff: %.5f, and calculated integral: %.5f\n", (double)tempduty, (double) diff, (double) integral);
    printf("Current angle: %.5f, giving diff of: %.5f from desired angle: %.5f\n", (double) current_angle, (double) diff, (double) degrees_C);

    return duty;
}

float get_wing_angle(bool left){ ///Will take in sensor values from WING accelerometer (LEFT or RIGHT depends on boolean)
    acceleration accel; 
    acceleration bird = get_bird_accel();

    // Check if bird needs reset because all outputs are 0
    if (!valid_magnitude(bird)) {
      return RESET_NUM_CENTER; // this error should pause the wings + reset motors 
    }

    // Check if center of bird is in a reasonable position
    if (bird.x < -2.0 || bird.x > 2.0 || 
        bird.y < -2.0 || bird.y > 2.0 || 
        bird.z < 9.0 || bird.z > 11.0) {
      
      // Return non-possible number because bird acceleration does not make sense (garbage value)
      // printf("CENTER RAW: %.5f %.5f %.5f \t", (double)bird.x, (double)bird.y, (double)bird.z);
      return ERROR_HALT;
    }

    float accel_x;
    float accel_y;
    float accel_z;
                                 
     // Accelerometer value is read (for the test done in the Python script, the pinout for the body accelerometer was used)
    if (left) {      //for left wing
       accel = get_raw_accel(LEFTWING); 
       // printf("LEFT RAW: LX: %.5f, LY: %.5f, LZ: %.5f, \t", (double)accel.x, (double)accel.y, (double)accel.z);
     } else {          //for right wing
       accel = get_raw_accel(RIGHTWING);
       // printf("RIGHT RAW: RX: %.5f, RY: %.5f, RZ: %.5f \t", (double)accel.x, (double)accel.y, (double)accel.z);
    }
    accel_x = accel.x;                         //X acceleration value from wing sensor
    accel_y = accel.y;                        //Y acceleration value from wing sensor 
    accel_z = accel.z;                        //Z acceleration value from wing sensor
    
    // If all accelerations are 0, return indicator to reset accelerometer

    // TODO: change to check for accelerometer magnitude 
    if (!valid_magnitude(accel)) {
      return RESET_NUM_WING;
    }

    // checking if wings are past -90/90 degrees
    if (accel_z < 0.0){                         
      // past setting: set_pwm(LEFT, UP, 0);
      // printf("Wing is past 90deg");
      return ERROR_HALT;
    }

    // This is to make sure the tangent calculation does not go to infinity
    if (float_equality(accel_z, 0.0)) {
      accel_z = 0.001;
    }

    if(left) {
      accel_x -= bird.x;
      accel_y -= bird.y;
    } else {
      accel_x += bird.x;
      accel_y += bird.y;
    }

    // bird_accel

    float tan_pitch =  accel_y/accel_z;               //Tangent and angle calculations performed. Similar to the calculations done in get_bird_angle()
    
    float angle_rads = atanf(tan_pitch);
    float angle_degrees = angle_rads * 180 / 3.14;

    // TODO: Why bird_angle different here than python?
    
    return angle_degrees * -1;
}

extern void common_hal_time_delay_ms(uint32_t);

//This function takes the degree setpoint from Python and calculates the necessary duty cycle to output. 
//Essentially the heart of the PID control loop.

void set_wing_angle(bool left, int degrees_C){ 
    float current_angle = get_wing_angle(left);

    // Check for errrors.
    if (float_equality(current_angle, ERROR_HALT)) {
      set_pwm(left, UP, 0);

      // if(left) {
      //   // printf("Current_angle: %.5f, left wing error; halting\t", (double)current_angle);
      //   // acceleration accel = get_raw_accel(LEFTWING); 
      //   // printf("LEFT RAW: %.5f %.5f %.5f \t", (double)accel.x, (double)accel.y, (double)accel.z);
      // }else {
      //   // printf("Current_angle: %.5f, right wing error; halting\n", (double)current_angle);
      //   // acceleration accel = get_raw_accel(RIGHTWING); 
      //   // printf("RIGHT RAW: %.5f %.5f %.5f \t", (double)accel.x, (double)accel.y, (double)accel.z);
      // }
      
      return;
    } else if (float_equality(current_angle, RESET_NUM_WING)) {
      set_pwm(left, UP, 0);
      
      // Need to delay a little or else i2c crashes (it seems like if we do two 
      // i2c commands in too quick of succession, then i2c starts acting funky)
      // Note: this is very similar to how i2c_accel_init has delays.
      // Note: python doesn't seem to have this issue because python is slow.
      // TODO: experiment with shrinking this delay.
      common_hal_time_delay_ms(1); 

      if(left) {
        accel_reset(LEFTWING);
      }else {
        accel_reset(RIGHTWING);
      }

      if(left) {
        printf("left wing reset\n");
      }else {
        printf("right wing reset\n");
      }
      
      return;
    }

    else if (float_equality(current_angle, RESET_NUM_CENTER)){
      set_pwm(left, UP, 0);

      common_hal_time_delay_ms(1); 
      accel_reset(CENTER);
      // printf("bird accel reset\n");
    }

    // Check for more errors.
    if (current_angle > 50.0 || current_angle < -50.0) {
      set_pwm(left, UP, 0);

      if(left) {
        // ("left wing out of bounds; halting\n");
      }else {
        // printf("right wing out of bounds; halting\n");
      }

      return;
    }

    float duty = pid_update_pwm(current_angle, degrees_C);

    // Execute the calculated duty cycle on given wing
    if (current_angle < degrees_C){
        if(left){ //send the PWM signal to move the left wing up
            printf("LEFT UP: GoalLeft: %d, current_angle_left: %.5f, duty_left: %.5f \t", degrees_C, (double)current_angle, (double) duty);
            set_pwm(LEFT,UP,duty);
        }
        else{ //send the PWM signal moves the right wing up 
            printf("RIGHT UP: GoalRight: %d, current_angle_right: %.5f, duty_right: %.5f \n", degrees_C, (double)current_angle, (double)duty);
            set_pwm(RIGHT,UP,duty);
        }

    } else {
        if(left){  //send a PWM signal to stop the left wing (TODO: program this so that it moves the left wing down)
            printf("LEFT STOP DOWN: Goal: %d, current_stop_angle_left: %.5f, duty_left: %.5f\t", degrees_C, (double)current_angle, (double) duty);
            set_pwm(LEFT, DOWN, duty);
        }
        else{      //send a PWM signal to stop the right wing (TODO: program this so that it moves the right wing down)
            printf("RIGHT STOP DOWN: Goal: %d, current_stop_angle_right: %.5f, duty_right: %.5f\n", degrees_C, (double)current_angle, (double) duty);
            set_pwm(RIGHT, DOWN, duty);
        }
    }
}
//    return duty;} //only used for testing if duty cycle values can be printed 
