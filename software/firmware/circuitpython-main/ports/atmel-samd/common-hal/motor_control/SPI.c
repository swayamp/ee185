#include "ports/atmel-samd/common-hal/motor_control/SPI.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#define SCK &pin_PA17 //SCK
#define MOSI &pin_PB23 //MOSI
#define MISO &pin_PB22 //MISO
#define CS_CENTER &pin_PA18 //Pin D9 on feather m4
#define SPI_BAUD 100000
#define SPI_POLARITY 0
#define SPI_PHASE 0
#define SPI_XTRA_CLK 0

static bool initted = false;


/*
    Checks if sensor has been initted. If not:
    Initializes the Feather M4 SPI bus, selected GPIO pin, and accelerometer object for a new sensor object
*/
void spi_init(spi_sensor_t* device){
    if(initted){
        return;
    }
    //init spi bus
    common_hal_busio_spi_construct(&device->bus, SCK, MOSI,MISO);

    //init chip select pins
    common_hal_digitalio_digitalinout_construct(&device->cs, CS_CENTER);
    common_hal_digitalio_digitalinout_switch_to_output(&device->cs, true, DRIVE_MODE_PUSH_PULL);

    //construct spidevices
    //These are the default values from shared-bindings/adafruit_bus_device/SPIDevice.c
    common_hal_adafruit_bus_device_spidevice_construct(&device->sensor, &device->bus, &device->cs, SPI_BAUD, SPI_POLARITY, SPI_PHASE, SPI_XTRA_CLK);
    initted = true;
}

//Writes to SPI bus, make sure the device has been "entered" into
static void write(spi_sensor_t *device, uint8_t *buffer, size_t length)
{
    common_hal_busio_spi_write(&device->bus, buffer, length);
}

//Reads from SPI bus into buffer
static void readinto(spi_sensor_t *device, uint8_t *buffer, size_t length)
{
    common_hal_busio_spi_read(&device->bus, buffer, length, 0);
}

//See https://github.com/adafruit/Adafruit_CircuitPython_LIS3DH/blob/master/adafruit_lis3dh.py
static void *_read_register (spi_sensor_t *device, uint8_t reg, size_t length)
{
    if (length == 1)
    {
        device->gbuf[0] = (reg | 0x80) & 0xFF;
    }
    else
    {
        device->gbuf[0] = (reg | 0xC0) & 0xFF;
    }
    //Enter before beginning the operation
    common_hal_adafruit_bus_device_spidevice_enter(&device->sensor);
    write(device, device->gbuf, 1);
    readinto(device, device->gbuf, length);
    common_hal_adafruit_bus_device_spidevice_exit(&device->sensor);
    //Exit after completing the operation
    return device->gbuf;
}

// Send a register and value to write within that register over the SPI bus using a buffer
void spi_write_register_byte(spi_sensor_t *device, uint8_t reg, uint8_t value)
{
    device->gbuf[0] = reg & 0x7F;
    device->gbuf[1] = value & 0xFF;
    common_hal_adafruit_bus_device_spidevice_enter(&device->sensor);
    write(device, device->gbuf, 2);
    common_hal_adafruit_bus_device_spidevice_exit(&device->sensor);
}

// Reads an individual byte from a 
uint8_t spi_read_register_byte(spi_sensor_t *device, uint8_t reg)
{
    return *(int *)_read_register(device, reg, 1);
}

