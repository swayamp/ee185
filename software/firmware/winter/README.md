# Firmware
This directory stores source code for the firmware that runs on a Fractal Flyer.
A FractalFlyer has a [Stanford Maxwell](https://github.com/maholli/sam32) board, 
which is a variant of the [Feather M4 Express
board](https://www.adafruit.com/product/3857). The firmware provides a [CircuitPython](https://circuitpython.org/)-based programming interface.

The goal of this project is to make the flyer software more efficient by placing a Python wrapper around C code that can calculate and set the colors of LEDs and change the wing positions.

## Getting Started

Need to add sections for Mac, Windows, and Linux. We also should include documentation for deploying code onto the board. 

### Mac

#### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

#### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
### Windows

#### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

#### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo
### Linux

#### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

#### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Creating C Modules in CircuitPython
1.  Follow the guide [here](https://learn.adafruit.com/extending-circuitpython/inside-the-virtual-machine) until it gets to the files in /ports/atmel-samd
2.  Edit py/circuitpy_mpconfig.h instead of ports/atmel-samd, and copy the format of the other modules where all the externs and modules are defined, as well as the MICROPY_PORT_BUILTIN_MODULES_STRONG_LINKS macro
3.  Edit py/circuitpy_defns.mk  instead of the Makefile, and follow the pattern with the other modules: add the module name to SRC_PATTERNS, and the c files to SRC_SHARED_MODULE_ALL

## Deployment

Add additional notes about how to deploy this on a live system

## Implementation Details

?? maybe this section?

## API

?? again maybe

## Built With

* [CircuitPython](https://circuitpython.readthedocs.io/en/6.0.x/README.html) - The (small) Python library on the Feather M4
* [NeoPixel](https://github.com/adafruit/Adafruit_CircuitPython_NeoPixel) - The CircuitPython library for communicating with the LED strips


## Authors

* **Abby Audet** 
* **Albert** 
* **Akwasi** 

## Updates

## To-Do's

- [X] Figure out how to make a check box
- [ ] Get CircuitPython in the GitLab repo
- [ ] Get the readme in the GitLab repo