# FLIGHT Simulator

The FLIGHT simulator is a Linux program that appears to be the FLIGHT
installation and responds to scripting commands. It is structured as
follows:

  - [fsimulator](fsimulator): the main executable for running the simulator.
  - [graphics](graphics): the graphics library for displaying the simulated
  state of FLIGHT.
  - [FlightGUI](../FlightGui): a simple test client for interacting with the simulator.

The intended use of the simulator is to test and visualize what the
[FlightGui](../FlightGui) UI is doing. It allows you to test its
output without requiring a full FLIGHT installation. This lets you
test whether the UI is producing correct scripts and debug more easily.

The graphics allow us to build a more richer, more realistic representation of
what the piece will look like before we install it and can allow us to model
different lighting conditions and take the dichroic of the flyers' wings into
account when simulating the LEDs and flapping of the wings.

## Operation

Normally, each Fractal Flyer in FLIGHT is plugged into a computer with
USB. Each Flyer appears to the computer in two ways, as a serial port
presenting a Python read-eval-print-loop and as a removable USB drive
that you can store Python code on.

The FLIGHT simulator recreates this exact behavior. When it boots, it
creates 76 entries in the local file system (named FIFOs) that act
like serial ports. These simulated serial ports are connected to
Python interpreters that provide the same API as a Fractal Flyer (see
below). The simulator also creates 76 directories that you can copy
Python code into, code which is accessible to the Python
interpreters. Just like the Fractal Flyers, if you copy new code to
the directory, the Python interpreter reboots: the simulator registers
for changes to the directory with `inotify(7)` and restarts the
corresponding interpreter process when it receives a notification.

These 76 interpreter processes send commands to a server process over
a network socket, which simulates the lights and motion of the Fractal
Flyers and displays them graphically. The high-level flow of the
simulator is summarized in the following diagram.

![The high level flow of the simulator is summarized in this diagram.](Simulator Flow Only.png)

The FlightEngine creates the Generator, and points it to the 76 FIFO
files whose paths are defined in outputs.json. The Generator then
outputs raw python scripts to each of the 76 FIFO files, corresponding
to the commands for each fractal flyer. The simulator flyer processes
then read from their respective FIFOs, interpret the python, and send
a text representation of the command over a C socket to a server
created in the Unity project (the graphics simulator). The graphics
simulator then receives this message and updates the graphics for
the flyer accordingly.

The job of the simulator is to set up this entire pipeline by
1. Creating the outputs.json file to configure the Generator.
2. Create the 76 FIFO files in the paths given by outputs.json.
3. Spawn the 76 processes that act like flyers.

The following diagram shows how fsimulator.cc creates
the individual components of the data pipeline.

![The high level flow of the simulator is summarized in this diagram.](SimulatorFlow.jpg)

![The simulator creates the data pipeline like so.](Simulator.png)

The simulation component of the simulator is complete and can be
integrated with the graphics.

## Python API

Fractal Flyers provide 3 Python functions to control their lights and wings:

``` Python
wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b,), (r, g, b,))
body_leds((r, g, b))
wing_angle(LEFT|RIGHT, angle)

```

Where r g and b are 0-255 and angle is -90.0 - 90.0.

This API assumes that the LED changes are near-instantaneous. The wing
angle change, however, may take time. Each Fractal Flyer has a local
control loop that moves the wings to the last specified position in a
stable way.

For example, suppose the left wing is at 0 and a script calls
`wing_angle(LEFT, -20)`. The left wing will start to lower to
-20. Suppose, when it is at -10, the script calls `wing_angle(LEFT,
-30)`.  The wing will keep on lowering until it reaches -30. If, when
it is at -25, the script calls `wing_angle(LEFT, 10)`, it will slow
and stop, then start moving to 10.  The firmware takes care of all of
this logic.

# Setting up FlightSimulator

Download and run FlightGUI, FSimulator + the Unity Project, and check the diagram flow to understand how the three parts interconnect

FlightGUI + Unity can be run on any platform; FSimulator must be run on Linux, so for best results, run FlightGUI + FSimulator together on the same computer, then run Unity on the same or different computer

As currently set up, all three programs must be running on the same network and if connected to a large network, like on Stanford's campus, also the same router (because we need the same subnet mask)

Make sure port 61053 (= 0xEE7D because 7D = 125) is not sending/receiving any signals on the computers FSimulator/Unity is working on, as this is the port that FSimulator will use to communicate with Unity and run