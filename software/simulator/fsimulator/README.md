# Flight Simulator 

This directory contains the main source code for the Flight simulator. To run
it, follow these steps:

  1. Install the Python library for simulating a Fractal Flyer in a Python interpreter: `python3 setup.py install`. You may need to run this with `sudo`.
  1. Compile the simulator: `make`.
  1. Run the simulator: `./fsimulator`.

The simulator exposes a simple console interface. 


## Files
  -fsimulator.cc: Main process for reading from engine output, spawning
76 processes, and transferring data into the main simulator process. 
 - simLib.c: The symbolic library containing C bindings for the engine's 
python API. 
 - api.py: A local version of the python API for use in binding incoming 
python commands to their simLib versions. 
 - server.cc: networking code that receives commands from the 76 Python interpreters.

### fsimulator.cc

This file contains the main code of the simulator which creates all 76 flyer emulations, runs the interpreters, and runs the server side implementation.  From here the process is able to send signals to shutdown or restart any interpreter and manage the entire simulation.  It creates the server followed by the flyer emulations.  Each emulation creates a FIFO.  All the FIFO are stored at /tmp/fractal_flyers/flyeri where i is the index of the fractal flyer.  If directory /tmp/fractal_flyers does not exist, it is created.  Both of these file structures are destroyed when the process is closed or crashes.  After creating the FIFOS the process then runs on an interpreter on a new process which reads from the FIFO and runs the commands.  Before this is done, a set of initalization python commands are run which connect to the server and communicate information about the fractal flyer.  The commands later sent on that flyer therefore have context that they are sent by a specific fractal flyer.  If the fractal flyer is in a while loop, the interpreter is closed and reopened.  The entire python environment and objects are destroyed and the interpreter is reopened, reconnects, and is then able again to read commands from the FIFO. While the interpreters are running, the main process is waiting on user input.  If the user types in -1 or ctrl-C a signal is sent to kill all of the processes, close the file structures, and cleanly close the program.  If the user types in any positive number, that flyer index is restarted.  This is intended to resolve any issues with inifinite while loops which can stall an interpreter indefinetly.  


simlib.c:  
Simlib defines the following python functions:

simlib.init(FF)
- creates socket and informs server of the FF associated with the socket

simlib.body_leds(FF,r,g,b)
- defines the body colors of the fractal flyer
- r,g,b are ints between 0 and 255

simlib.wing_leds(FF,direction,(r1,g1,b1),(r2,g2,b2),(r3,g3,b3))
- defines the three colors of the wing specific by direction of the FF specifiied by FF
- Left = 1; Right = 0;
- r,g,b are ints between 0 and 255

simlib.wing_angle(FF,direction,angle)
- defines the angle of the wing specific by direction of the FF specifiied by FF
- Left = 1; Right = 0;
- angle is a floating point number

api.py:  
Not used currently.  No longer neccessary
