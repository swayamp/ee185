#ifndef FLIGHT_SERVER_H
#define FLIGHT_SERVER_H

struct flight_rgb {
    int red;
    int green;
    int blue;
};
struct flight_wing {
    struct flight_rgb node1;
    struct flight_rgb node2;
    struct flight_rgb node3;
    double angle;
};
struct flight_flyer {
    int number;
    struct flight_rgb body;
    struct flight_wing leftWing;
    struct flight_wing rightWing;
};

/* 
 * Starts the network server, which listens to commands from all
 * of the Flyer Python interpreter processes and incorporates them
 * into the current state of FLIGHT.
 */
int flight_server_start();


/* Returns the number of Fractal Flyers in the simulation. */
int flight_server_num_flyers();

/* Returns the state of a particular Fractal Flyer. Returns NULL if 
 * which is not a valid Fractal Flyer. */
struct flight_flyer* flight_server_get_flyer(int which);

#endif
