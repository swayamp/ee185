#include <Python.h>
// Client side C/C++ program from geeksforgeeks.org
#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <errno.h>
#include <stdbool.h>
#define PORT 8080 
   
int openSocket(){
	int sock = 0;
  struct sockaddr_in serv_addr; 

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
  { 
      printf("\n Socket creation error \n"); 
      return -1; 
  } 

  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int)) < 0) 
  {
    printf("setsockopt(SO_REUSEADDR) failed");
  }



  serv_addr.sin_family = AF_INET; 
  serv_addr.sin_port = htons(PORT); 
     
  // Convert IPv4 and IPv6 addresses from text to binary form 
  if(inet_pton(AF_INET, "0.0.0.0", &serv_addr.sin_addr)<=0)  
  { 
      printf("\nInvalid address/ Address not supported \n"); 
      return -1; 
  } 
 
  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
  { 
      printf("\nConnection Failed %s \n", strerror(errno)); 
      return -1; 
  } 
  return sock;
}

/* Reads in id and one tuple of rgb colors */
static PyObject *body_leds(PyObject *self, PyObject *args) {
    int r,g,b,FF;
    PyObject* rgb_tuple;

    /* Parse arguments */
    if (!PyArg_ParseTuple(args, "iO", &FF, &rgb_tuple)) {
      return NULL;
    }

    if(!PyArg_ParseTuple(rgb_tuple, "iii", &r, &g, &b)) {
        return NULL;
    }

    int sock = *(int*)PyModule_GetState(self);

    if(sock == -1)
    	return PyLong_FromLong(-1);
    char message_send[1024];
    sprintf(message_send,"%d:1:%d,%d,%d",FF,r,g,b); 
    char message_recieved[1024] = {0}; 

    send(sock , message_send , strlen(message_send) , 0 ); 
    printf("Sent message Len: %lu\n",strlen(message_send)); 
    printf("Message sent: %s\n",message_send); 
    // int valread = read( sock , message_recieved, 1024); 
    // printf("Recieved message Len: %d\n",valread);
    // printf("Message Recieved: %s\n",message_recieved); 
    return PyLong_FromLong(0); 
}
/* reads in id, direction as bool and 3 tuples of rgb colors */
static PyObject *wing_leds(PyObject *self, PyObject *args) {
    int r1,g1,b1, r2,g2,b2, r3,g3,b3, FF;
    bool direction;
    PyObject* tuple1;
    PyObject* tuple2;
    PyObject* tuple3;

    /* Parse arguments */
    if(!PyArg_ParseTuple(args, "ipOOO", &FF, &direction, &tuple1, &tuple2, &tuple3)) {
        return NULL;
    }
    if(!PyArg_ParseTuple(tuple1, "iii", &r1, &g1,&b1)) {
        return NULL;
    }
    if(!PyArg_ParseTuple(tuple2, "iii", &r2, &g2,&b2)) {
        return NULL;
    }
    if(!PyArg_ParseTuple(tuple3, "iii", &r3, &g3,&b3)) {
        return NULL;
    }

    int sock = *(int*)PyModule_GetState(self);

    if(sock == -1)
    	return PyLong_FromLong(-1);
    char message_send[1024];
    sprintf(message_send,"%d:2:%d:%d,%d,%d:%d,%d,%d:%d,%d,%d",FF,direction,r1,g1,b1,
    																       r2,g2,b2,
    																	   r3,g3,b3); 
    char message_recieved[1024] = {0}; 

    send(sock , message_send , strlen(message_send) , 0 ); 
    printf("Sent message Len: %lu\n",strlen(message_send)); 
    printf("Message sent: %s\n",message_send); 
    return PyLong_FromLong(0); 
}

/* Reads in wing id, angle, and bool for direction */
static PyObject *wing_angle(PyObject *self, PyObject *args) {
    int FF, direction;
    float angle;

    /* Parse arguments */
    if(!PyArg_ParseTuple(args, "iif", &FF, &direction, &angle)) {
        return NULL;
    }

    int sock = *(int*)PyModule_GetState(self);

    if(sock == -1)
    	return PyLong_FromLong(-1);
    char message_send[1024];
    sprintf(message_send,"%d:3:%d:%f",FF,direction,angle); 
    char message_recieved[1024] = {0}; 

    send(sock , message_send , strlen(message_send) , 0 ); 
    printf("Sent message Len: %lu\n",strlen(message_send)); 
    printf("Message sent: %s\n",message_send); 
    return PyLong_FromLong(0); 
}

/* Takes in FF id and creates a socket that the server
 * understands has the same id.  Socket is stored in
 * module state
 */
static PyObject *init(PyObject *self, PyObject *args) {
    int sock = openSocket();
    *(int*)PyModule_GetState(self) = sock;
    int FF;

    if(!PyArg_ParseTuple(args, "i",&FF)) {
        return NULL;
    }
    char message_send[1024];
    sprintf(message_send,"%d",FF); 

    send(sock , message_send , strlen(message_send) , 0 ); 
    printf("Sent message Len: %lu\n",strlen(message_send)); 
    printf("Message sent: %s\n",message_send);
    return PyLong_FromLong(0);
}

/* Unneccessary destroy call 
 */
static PyObject *destroy(PyObject *self, PyObject *args) {
    int sock = *(int*)PyModule_GetState(self);
    send(sock , "EOF", strlen("EOF") , 0 ); 
    return PyLong_FromLong(0);
}

static PyMethodDef simLibMethods[] = {
    {"body_leds", body_leds, METH_VARARGS, "Python interface for body Led communication with Simulator"},
    {"wing_leds", wing_leds, METH_VARARGS, "Python interface for body Led communication with Simulator"},
    {"wing_angle", wing_angle, METH_VARARGS, "Python interface for body Led communication with Simulator"},
    {"init", init, METH_VARARGS, "Python interface for body Led communication with Simulator"},
    {"destroy", destroy, METH_VARARGS, "Python interface for body Led communication with Simulator"}
};

static struct PyModuleDef simLibmodule = {
    PyModuleDef_HEAD_INIT,
    "simLib",
    "Python interface for the Simulation C library function",
    4,
    simLibMethods
};


PyMODINIT_FUNC PyInit_simLib(void) {
    return PyModule_Create(&simLibmodule);
}

