﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Text;

// Contact me at nirvikb@stanford.edu if you have questions about the simulator

public class FlightMasterController : MonoBehaviour
{
    // Numbers representing wing ids
    const int LEFT = 0;
    const int RIGHT = 1;

    public GameObject flyerObj;
    private GameObject[] flyers;
    private Coroutine[] flyerAnimators;
    public int MAX_FLYERS = 76;

    private TcpListener tcpListener; 
    private Thread tcpListenerThread;   
    private TcpClient connectedTcpClient;   

    void Awake() {
        flyerAnimators = new Coroutine[MAX_FLYERS * 2];
        flyers = gameObject.GetComponent<InitialiseFlyers>().CreateFlyers(MAX_FLYERS, this);

        UnityThread.initUnityThread();
        Thread t = new Thread(()=>provideServerSocket());
        t.Start();
    }

    // TODO: you will have to clean up this socket code - if you run it in the debugger you'll notice that multiple comamnds get appended to the same line
    //       i believe it has something to do with the UnityThread.executeinupdate command...
    public void provideServerSocket(){
        TcpListener serverSocket = new TcpListener(8080);
        TcpClient clientSocket = default(TcpClient);
        int counter = 0;
        try
        {
            serverSocket.Start();
            UnityThread.executeInUpdate(() =>
            {
                Debug.Log("Server started");
            });
            counter = 0;
            while (true)
            {
                counter += 1;
                clientSocket = serverSocket.AcceptTcpClient();
                UnityThread.executeInUpdate(() =>
                {
                    Debug.Log(" >> " + "Client No:" + Convert.ToString(counter) + " started!");
                });
                handleClient client = new handleClient();
                client.startClient(clientSocket, Convert.ToString(counter), this);
            }
        }
        catch (Exception e)
        {
            UnityThread.executeInUpdate(() =>
            {
                Debug.Log("Error!" + e);
            });
            throw e;
        }
    }

    public class handleClient
    {
        TcpClient clientSocket;
        FlightMasterController masterDevice;
        string clNo;
        public void startClient(TcpClient inClientSocket, string clineNo, FlightMasterController master)
        {
            masterDevice = master;
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;
            Thread ctThread = new Thread(doChat);
            ctThread.Start();
        }
        private void doChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[4096];
            string dataFromClient = null;
            string serverResponse = null;
            string rCount = null;
            requestCount = 0;

            while ((true))
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    networkStream.Read(bytesFrom, 0, bytesFrom.Length);
                    dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    Debug.Log(dataFromClient);
                    UnityThread.executeInUpdate(() =>
                    {
                        masterDevice.ReadString(dataFromClient);
                    });
                }
                catch (Exception ex)
                {
                    UnityThread.executeInUpdate(() =>
                    {
                        Debug.Log(" >> " + ex.ToString() + dataFromClient);
                    });
                }
            }
        }
    } 


    public void ReadString(string line)
    {
        char[] charsToTrim = {',', ':'};
        string[] args = line.Split(charsToTrim);
        int flyerId;
        float arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9;
        if(Int32.TryParse(args[0], out flyerId)) {
            if (flyerId < flyers.Length && flyerId >= 0 && args.Length >= 2) {
                GameObject flyer = flyers[flyerId];
                if (args[1] == "1" && args.Length >= 5 && float.TryParse(args[2], out arg1) && float.TryParse(args[3], out arg2) && float.TryParse(args[4], out arg3)) { // Change body LEDs
                    changeColor(flyer, new Color(arg1/255f, arg2/255f, arg3/255f), new Color(arg1/255f, arg2/255f, arg3/255f), new Color(arg1/255f, arg2/255f, arg3/255f), "2");
                } else if (args[1] == "2" && args.Length >= 6 
                                          && float.TryParse(args[3], out arg1) && float.TryParse(args[4], out arg2) && float.TryParse(args[5], out arg3)
                                          && float.TryParse(args[6], out arg4) && float.TryParse(args[7], out arg5) && float.TryParse(args[8], out arg6)
                                          && float.TryParse(args[9], out arg7) && float.TryParse(args[10], out arg8) && float.TryParse(args[11], out arg9)) { // Change wing LEDs
                    changeColor(flyer, new Color(arg1/255f, arg2/255f, arg3/255f), new Color(arg4/255f, arg5/255f, arg6/255f), new Color(arg7/255f, arg8/255f, arg9/255f), args[2]);
                } else if (args[1] == "3" && args.Length >= 4 && float.TryParse(args[3], out arg1)) { // Change wing angle
                    moveWing(flyer, args[2], arg1, flyerId);
                }
            }
        }
    }


    void moveWing(GameObject flyer, string wingId, float angle, int flyerId) {
        GameObject leftWing = flyer.transform.Find("WingLeft").transform.Find("Pivot").gameObject;
        GameObject rightWing = flyer.transform.Find("WingRight").transform.Find("Pivot").gameObject;

        // Fix extreme angles for lerp purposes
        if (angle >= 90) { 
            angle = 89.9f;
        } else if (angle <= -90) {
            angle = -89.9f;
        }

        if (wingId == "0") { // Right
            float z = rightWing.transform.eulerAngles.z;
            float x = rightWing.transform.eulerAngles.x;
            float y = rightWing.transform.eulerAngles.y;
            if (flyerAnimators[flyerId * 2 + RIGHT] != null) {
                StopCoroutine(flyerAnimators[flyerId * 2 + RIGHT]);
            }
            flyerAnimators[flyerId * 2 + RIGHT] = StartCoroutine(Rotate(rightWing, new Vector3(angle - x, 0, 0), 0.5f));
        } else { // Left
            float z = leftWing.transform.eulerAngles.z;
            float x = leftWing.transform.eulerAngles.x;
            float y = leftWing.transform.eulerAngles.y;
            if (flyerAnimators[flyerId * 2 + LEFT] != null) {
                StopCoroutine(flyerAnimators[flyerId * 2 + LEFT]);
            }

            flyerAnimators[flyerId * 2 + LEFT] = StartCoroutine(Rotate(leftWing, new Vector3(x - angle, 0, 0), 0.5f));
        }
    }

    // Since there are three lights in each component, need to set all of them
    void changeColor(GameObject flyer, Color topColor, Color tipColor, Color bottomColor, string elementId) 
    {
        // Change colours
        if (elementId == "2") { // Changing body lights
            Light bodyLightFront = flyer.transform.Find("Body").GetChild(1).gameObject.GetComponent<Light>();
            Light bodyLightMid = flyer.transform.Find("Body").GetChild(2).gameObject.GetComponent<Light>();
            Light bodyLightBack = flyer.transform.Find("Body").GetChild(1).gameObject.GetComponent<Light>();

            bodyLightFront.color = topColor;
            bodyLightMid.color = tipColor;
            bodyLightBack.color = bottomColor;
        } else if (elementId == "1") { // Changing left lughts
            Light leftLightTop = flyer.transform.Find("WingLeft").transform.Find("Pivot").GetChild(1).gameObject.GetComponent<Light>();
            Light leftLightTip = flyer.transform.Find("WingLeft").transform.Find("Pivot").GetChild(2).gameObject.GetComponent<Light>();
            Light leftLightBottom = flyer.transform.Find("WingLeft").transform.Find("Pivot").GetChild(3).gameObject.GetComponent<Light>();
            leftLightTop.color = topColor;
            leftLightTip.color = tipColor;
            leftLightBottom.color = bottomColor;

        } else { // Changing right lights
            Light rightLightTop = flyer.transform.Find("WingRight").transform.Find("Pivot").GetChild(1).gameObject.GetComponent<Light>();
            Light rightLightTip = flyer.transform.Find("WingRight").transform.Find("Pivot").GetChild(2).gameObject.GetComponent<Light>();
            Light rightLightBottom = flyer.transform.Find("WingRight").transform.Find("Pivot").GetChild(3).gameObject.GetComponent<Light>();
            rightLightTop.color = topColor;
            rightLightTip.color = tipColor;
            rightLightBottom.color = bottomColor;
        }
    }

    private IEnumerator Rotate(GameObject objectToRotate, Vector3 angles, float duration ) {
        Quaternion startRotation = objectToRotate.transform.localRotation ;
        Quaternion endRotation = Quaternion.Euler( angles ) * startRotation ;
        for( float t = 0 ; t < duration ; t+= Time.deltaTime )
        {
            objectToRotate.transform.localRotation = Quaternion.Lerp( startRotation, endRotation, t / duration ) ;
            yield return null;
        }
        objectToRotate.transform.localRotation = endRotation;
    }

}
