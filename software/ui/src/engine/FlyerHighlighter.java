package engine;
import heronarts.lx.LX;
import heronarts.lx.color.LXColor;
import heronarts.lx.effect.LXEffect;
import heronarts.lx.parameter.BooleanParameter;
import heronarts.lx.parameter.DiscreteParameter;
import heronarts.lx.parameter.LXParameter;
import heronarts.lx.modulator.SquareLFO;

import java.util.List;

import model.FlightModel;
import model.FlyerConfig;
import model.FlyerModel;
import model.LightSamplePointModel;

public class FlyerHighlighter extends LXEffect {

  public final FlyerConfig[] flyerConfigurations;
  public final SquareLFO strobe = new SquareLFO(50, 100, 500);
  public final DiscreteParameter flyerIndex;
  public final BooleanParameter showBlanks = new BooleanParameter("BLANKS", false);
  public final FlightModel model;

  public FlyerHighlighter(LX lx, FlyerConfig[] configs, FlightModel model) {
    super(lx);
    this.model = model;
    flyerConfigurations = configs;
    flyerIndex = new DiscreteParameter("FLYER", flyerConfigurations.length);
    // REVIEW(mcslee): add the parameter to the component, and your effect
    // will automatically be registered as a listener
    addParameter("flyerIndex", flyerIndex);
    // flyerIndex.addListener(new LXParameterListener() {
    //   public void onParameterChanged(LXParameter parameter) {
    //     strobe.setStartValue(100);
    //     strobe.trigger();
    //   }
    // });
    addModulator(strobe).start();
    disable();
  }

  // REVIEW(mcslee): automatically receive these changes from our
  // registered parameter
  @Override
  public void onParameterChanged(LXParameter p) {
    if (p == flyerIndex) {
      strobe.setStartValue(100);
//      strobe.trigger();
    } 
  }

  public FlyerConfig getConfig() {
    return flyerConfigurations[flyerIndex.getValuei()];
  }

  public FlyerModel getFlyer() {
    return model.getFlyer(flyerIndex.getValuei());
  }

  public List<LightSamplePointModel> getFlyerLightPoints() {
    return model.getFlyerLights(flyerIndex.getValuei());
  }

  public void reloadModel() {
      lx.getModel().bang();
  }

  public void trigger() {
      //strobe.trigger();
  }

  @Override
  public void run(double deltaMs, double enabledAmount) {
    // REVIEW(mcslee): looks redunant with HighlightLayer below?
    if (isEnabled()) {
     for (LightSamplePointModel point : getFlyerLightPoints()) {
        int oldColor = getColor(point);
        float val = strobe.getValuef();
        int highlightColor = LX.hsb(0, 0, val);
        int newColor = LXColor.blend(oldColor,
                                     highlightColor,
                                     LXColor.Blend.ADD);
        setColor(point, newColor);
      }
    }
  }
}
