package engine;
//package src;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.time.*;

import model.FlightModel;
import model.FlyerModel;
import model.Geometry;
import model.LightSamplePointModel;

import java.io.DataInput;
import heronarts.lx.LX;
import heronarts.lx.output.LXOutput;
import java.awt.Color;



/*
 * Generates raw python commands (Strings) for all of the 76 fractal flyers. These commands
 * are currently written to 76 FIFO files defined by the outputs.json file (see simulator
 * directory README for more details). The rate that these commands are generated
 * can be controlled using a slider in the FlightGUI, and that rate defines the rate of
 * a token-bucket algorithm implementation. The Generator also compares the current state
 * of a flyer to it's previously generated state and ensures that duplicate commands are
 * not sent. You can email jflat@stanford.edu with any questions.
 */
public class Generator extends LXOutput implements Geometry {
  final FlightModel flight;
  final List<FlyerModel> flyers;
  final DataInput[] in;
  final PrintStream[] out;

  protected int[] frameColors;

  /* These instance variables are used in the token-bucket algorithm that
     limits/defines the rate of script generation. */
  static final int MAX_TOKENS = 5;    /* Max tokens (largest burst size). */
  Instant timer;  /* Used to determine time intervals for adding tokens. */
  double rate;    /* Rate of update in Hz. */
  long period;    /* Period of update in ns (one token added every period). */
  int tokens;     /* Current number of tokens available. */

  /* These instance variables are used to store previous states of the flyers
     so that duplicate commands are not sent. */
  Integer prev_lwing[] = new Integer[76];           /* Previous left wing angles. */
  Integer prev_rwing[] = new Integer[76];           /* Previous right wing angles. */
  Color prev_lleds[][] = new Color[3][76];  /* Previous left led colors as Color objects. */
  Color prev_rleds[][] = new Color[3][76];  /* Previous right led colors as Color objects. */
  Color prev_body[] = new Color[76];        /* Previous body led colors as Color object. */
  boolean force_send;                       /* True forces potentially duplicate commands to be sent. */
  boolean enabled;                         /* Linked to UI button to turn generator on/off. */

  static final String LEFT_WING_POS = "wing_angle(LEFT, %s)";
  static final String RIGHT_WING_POS = "wing_angle(RIGHT, %s)";
  static final String LEFT_WING_LEDs = "wing_leds(LEFT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))";
  static final String RIGHT_WING_LEDs = "wing_leds(RIGHT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))";
  static final String BODY_LED = "body_leds((%d, %d, %d))";


  public Generator(LX lx, FlightModel flight, DataInput[] inSockets, PrintStream[] outSockets) {
    super(lx);
    this.flight = flight;
    this.flyers = this.flight.getFlyers();
    this.in = inSockets;
    this.out = outSockets;
    this.timer = Instant.now();
    this.tokens = 0;
    this.rate = 1.0;
    this.period = 1000000000;
    this.enabled = false;
    this.force_send = true;
  }

  private Color getRGBColor(LightSamplePointModel point) {
    return new Color(this.frameColors[point.getIndex()]);
  }

  private Color[] getRGBColors(List<LightSamplePointModel> points) {
    // REVIEW(mcslee) - unclear benefit to making dynamic copies with java.awt.Color on every frame...
    // consider using int[] and you have the LXColor.red()/LXColor.green()/LXColor.blue() methods
    // which are simlar to java.awt.Color.getRed(), etc.
    //
    // There's no fundamental problem with what you're doing here, but you're just going to be putting
    // the garbage collector to work
	  Color[] ledColors = new Color[points.size()];
	  for (int i = 0; i < points.size(); i++) {
	    ledColors[i] = new Color(this.frameColors[points.get(i).getIndex()]);
	  }
	  return ledColors;
  }

  /*
   * ## Python API

		For a Python API, I’d suggest these three functions:

		wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b), (r, g, b))
		body_leds((r, g, b))
		wing_angle(LEFT|RIGHT, angle)
   */

  /* Generates the commands for a given flyer by analyzing the current state of the
     flyer from the FlightModel. Checks if the flyer's state has changed and only
     sends the relevant updated commands. */
  private ArrayList<String> generateCommands(int flyerIndex) {
    /*
     * String s = "hello %s!";
     * s = String.format(s, "world");
     * assertEquals(s, "hello world!");
     */
	  Integer leftWingPos = flight.getFlyerLeftWingSkew(flyerIndex);
	  Integer rightWingPos = flight.getFlyerRightWingSkew(flyerIndex);

  	LightSamplePointModel bodyLightPoint = flight.getFlyerBodyLight(flyerIndex);


	  List<LightSamplePointModel> leftLEDs = flight.getFlyerLeftWingLights(flyerIndex);
	  List<LightSamplePointModel> rightLEDs = flight.getFlyerRightWingLights(flyerIndex);

    ArrayList<String> commands = new ArrayList<String>();

    Color[] leftLights = getRGBColors(leftLEDs);
    Color[] rightLights = getRGBColors(rightLEDs);
    Color bodyLED = getRGBColor(bodyLightPoint);

    if (compareAndUpdate(leftWingPos, prev_lwing, flyerIndex)) {
      commands.add(String.format(LEFT_WING_POS, leftWingPos));
    }

    if (compareAndUpdate(rightWingPos, prev_rwing, flyerIndex)) {
      commands.add(String.format(RIGHT_WING_POS, rightWingPos));
    }

    if (compareAndUpdate(bodyLED, prev_body, flyerIndex)) {
      commands.add(String.format(BODY_LED,
          bodyLED.getRed(), bodyLED.getGreen(), bodyLED.getBlue()));
    }

    boolean addCommand = false;
    for (int i = 0; i < 3; i++) {
      if (compareAndUpdate(leftLights[i], prev_lleds[i], flyerIndex)) addCommand = true;
    }
    if (addCommand) {
      commands.add(String.format(LEFT_WING_LEDs,
        leftLights[0].getRed(), leftLights[0].getGreen(), leftLights[0].getBlue(),
        leftLights[1].getRed(), leftLights[1].getGreen(), leftLights[1].getBlue(),
        leftLights[2].getRed(), leftLights[2].getGreen(), leftLights[2].getBlue())
      );
    }

    addCommand = false;
    for (int i = 0; i < 3; i++) {
      if (compareAndUpdate(rightLights[i], prev_rleds[i], flyerIndex)) addCommand = true;
    }
    if (addCommand) {
      commands.add(String.format(LEFT_WING_LEDs,
        rightLights[0].getRed(), rightLights[0].getGreen(), rightLights[0].getBlue(),
        rightLights[1].getRed(), rightLights[1].getGreen(), rightLights[1].getBlue(),
        rightLights[2].getRed(), rightLights[2].getGreen(), rightLights[2].getBlue())
      );
    }

    commands.add("\n");

    return commands;
  }

  /* Generics function that is used to check if a previous Flyer state has changed,
     and updates the specific state (e.g. body leds, wing angle, etc.) if so. */
  private <T> boolean compareAndUpdate(T newValue, T[] currentState, int flyerIndex) {
    if (!newValue.equals(currentState[flyerIndex]) || force_send) {
      currentState[flyerIndex] = newValue;
      return true;
    }
    return false;
  }

  /* Used to set the rate from the slider in the FlightGUI. */
  protected void setRate(Double frequency) {
    if (frequency < 0.1) frequency = 0.1;
    this.rate = frequency;
    this.period = (long)(1000000000.0 / frequency);
  }

  /* Used to set enabled/disabled from the button in the FlightGUI. */
  protected void setEnabled(boolean enabled) {
    this.force_send = enabled;
    this.enabled = enabled;
  }

  /* Called by the FlightGUI very rapidly to update Flyer led colors and wing position.
     The script generation rate is then regulated by a token-bucket algorithm, and the
     rate can be set in the GUI. */
  @Override
  protected void onSend(int[] colors, double brightness) {
    //    super.onSend(colors, brightness);
    
    if (!enabled) return;
    long waitTime = Duration.between(timer, Instant.now()).toNanos();
    if (waitTime > period) {
      if (tokens < MAX_TOKENS) {
        tokens = Math.min(MAX_TOKENS, tokens + (int)(waitTime/period));
      }
      timer = Instant.now();
    }
    if (tokens < 1) return;
    tokens--;
    // REVIEW(mcslee): here we are being given the engine's composited and mixed
    // colors for this frame
    this.frameColors = colors;

    for (int i=0; i<flyers.size(); i++) {
      ArrayList<String> commands = generateCommands(i);
      for (String command : commands) {
        try {
          out[i].println(command);
        } catch (Exception e) {
          System.out.println("failed to write command!");
        }
      }
    }
    force_send = false;
  }

  @Override
  protected void onSend(int[] arg0, byte[] arg1) {
    throw new UnsupportedOperationException("LXDatagramOutput does not implement onSend by glut");
  }

}
