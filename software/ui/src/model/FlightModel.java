package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXModelBuilder;
import heronarts.lx.model.LXPoint;

public class FlightModel extends LXModel implements Flight {
  /**
   * Flyers in the model
   */
  protected final FlyerModel[] flyers;

  /**
   * Wings in the model
   */
  protected final WingModel[] wings;

  /**
   * lightPoints in the model, note that these are also in the
   * raw LXModel.points field, but lightPoints gives us access
   * to extra meta-data fields
   */
  protected final LightSamplePointModel[] lightPoints;

  FlightModel(FlightModelBuilder flightModelBuilder) {
    super(flightModelBuilder);
    this.flyers = flightModelBuilder.flyers.toArray(new FlyerModel[0]);
    this.wings = flightModelBuilder.wings.toArray(new WingModel[0]);
    this.lightPoints = flightModelBuilder.lightPoints.toArray(new LightSamplePointModel[0]);
  }

  public FlightModel(FlyerConfig[] flyerConfigs) {
    this(new FlightModelBuilder(flyerConfigs));
  }

  private static class FlightModelBuilder extends LXModelBuilder {

    private final List<FlyerModel> flyers = new ArrayList<FlyerModel>();
    private final List<WingModel> wings = new ArrayList<WingModel>();
    private List<LightSamplePointModel> lightPoints = new ArrayList<LightSamplePointModel>();

    public FlightModelBuilder(FlyerConfig[] flyerConfigs) {
      if (flyerConfigs.length != NUM_FLYERS) {
        throw new IllegalArgumentException("Wrong number of flyers in config, expected " + NUM_FLYERS + " but got: " + flyerConfigs.length);
      }
      for (FlyerConfig flyerConfig : flyerConfigs) {
        FlyerModel flyer = new FlyerModel(flyerConfig);
        flyers.add(flyer);
        for (LXPoint p : flyer.points) {
          addPoint(p);
        }

        // Build up the list of all wings and add light points
        for (WingModel wing : flyer.getWings()) {
          this.wings.add(wing);
          for (LightSamplePointModel lightPoint : wing.lightPoints) {
            lightPoints.add(lightPoint);
          }
        }

        // Add the flyer's body light point
        lightPoints.add(flyer.getBodyLightPoint());
      }
    }
  }

  /* ------------- all flyers, motion ------------- */

  @Override
  public List<WingModel> getAllWings() {
    return IntStream
      .range(0, wings.length)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<WingModel> getAllLeftWings() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % NUM_WINGS_PER_FLYER == 0)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<WingModel> getAllRightWings() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % NUM_WINGS_PER_FLYER == 1)
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public int[] getAllWingsSkew() {
    return IntStream
      .range(0, wings.length)
      .map(i -> wings[i].getSkew())
      .toArray();
  }

  @Override
  public int[] getAllLeftWingsSkew() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % NUM_WINGS_PER_FLYER == 0)
      .map(i -> wings[i].getSkew())
      .toArray();
  }

  @Override
  public int[] getAllRightWingsSkew() {
    return IntStream
      .range(0, wings.length)
      .filter(i -> i % NUM_WINGS_PER_FLYER == 1)
      .map(i -> wings[i].getSkew())
      .toArray();
  }

  /* ------------- all flyers, lights ------------- */


  @Override
  public List<LightSamplePointModel> getAllLights() {
    return Arrays.asList(lightPoints);
  }

  @Override
  public List<LightSamplePointModel> getAllBodyLights() {
    return IntStream
      .range(0, flyers.length)
      .mapToObj(i -> lightPoints[bodyLightIndex(i)])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getAllWingsLights() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(isWingLight)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getAllWingsPositionIndexedLights(int positionIndex) {
    return IntStream
      .range(0, wings.length)
      // (wingIndex / 2) -> flyerIndex, due to integer division rounding down
      // (wingIndex % 2 == 1) -> false/true, left/right
      .mapToObj(i -> lightPoints[wingLightIndex(i / 2, i % 2 == 1, positionIndex)])
      .collect(Collectors.toList());
  }


  @Override
  public List<LightSamplePointModel> getAllLeftWingLights() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(isLeftWingLight)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getAllLeftWingPositionIndexedLights(int positionIndex) {
    return IntStream
      .range(0, flyers.length)
      .mapToObj(i -> lightPoints[wingLightIndex(i, false, positionIndex)])
      .collect(Collectors.toList());
  }


  @Override
  public List<LightSamplePointModel> getAllRightWingLights() {
    return IntStream
      .range(0, lightPoints.length)
      .filter(isRightWingLight)
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getAllRightWingPositionIndexedLights(int positionIndex) {
    return IntStream
      .range(0, flyers.length)
      .mapToObj(i -> lightPoints[wingLightIndex(i, true, positionIndex)])
      .collect(Collectors.toList());
  }


  /* ------------- one flyer, lights ------------- */
  @Override
  public List<LightSamplePointModel> getFlyerLights(int flyerIndex) {
    return IntStream
      .range(flyerLightStartIndex(flyerIndex), flyerLightStartIndex(flyerIndex + 1))
      .mapToObj(i -> lightPoints[i])
      .collect(Collectors.toList());
  }

  @Override
  public LightSamplePointModel getFlyerBodyLight(int flyerIndex) {
      return lightPoints[bodyLightIndex(flyerIndex)];
  }

  @Override
  public List<LightSamplePointModel> getFlyerWingsLights(int flyerIndex) {
    return IntStream
      .range(0, NUM_WING_LIGHT_POINTS)
      .mapToObj(i -> lightPoints[flyerLightStartIndex(flyerIndex) + i])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getFlyerLeftWingLights(int flyerIndex) {
    return IntStream
      .range(0, NUM_LIGHT_POINTS_PER_WING)
      .mapToObj(i -> lightPoints[wingLightIndex(flyerIndex, false, i)])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getFlyerRightWingLights(int flyerIndex) {
    return IntStream
      .range(0, NUM_LIGHT_POINTS_PER_WING)
      .mapToObj(i -> lightPoints[wingLightIndex(flyerIndex, true, i)])
      .collect(Collectors.toList());
  }

  @Override
  public List<LightSamplePointModel> getFlyerWingsPositionIndexedLights(int flyerIndex, int positionIndex) {
    return IntStream
      .range(0, 2)
      .mapToObj(i -> lightPoints[wingLightIndex(flyerIndex, i == 1, positionIndex)])
      .collect(Collectors.toList());
  }

  @Override
  public LightSamplePointModel getFlyerLeftWingPositionIndexedLights(int flyerIndex, int positionIndex) {
      return lightPoints[wingLightIndex(flyerIndex, false, positionIndex)];
  }

  @Override
  public LightSamplePointModel getFlyerRightWingPositionIndexedLights(int flyerIndex, int positionIndex) {
      return lightPoints[wingLightIndex(flyerIndex, true, positionIndex)];
  }

  /* ------------- one flyer, motion ------------- */
  @Override
  public List<WingModel> getFlyerWings(int flyerIndex) {
    return IntStream
      .range(wingIndex(flyerIndex, false), wingIndex(flyerIndex + 1, false))
      .mapToObj(i -> wings[i])
      .collect(Collectors.toList());
  }

  @Override
  public WingModel getFlyerLeftWing(int flyerIndex) {
    return wings[wingIndex(flyerIndex, false)];
  }

  @Override
  public WingModel getFlyerRightWing(int flyerIndex) {
    return wings[wingIndex(flyerIndex, true)];
  }

  @Override
  public int[] getFlyerWingsSkew(int flyerIndex) {
    return IntStream
      .range(wingIndex(flyerIndex, false), wingIndex(flyerIndex + 1, false))
      .map(i -> wings[i].getSkew())
      .toArray();
  }

  @Override
  public int getFlyerLeftWingSkew(int flyerIndex) {
    return wings[wingIndex(flyerIndex, false)].getSkew();
  }

  @Override
  public int getFlyerRightWingSkew(int flyerIndex) {
    return wings[wingIndex(flyerIndex, true)].getSkew();
  }


  /* ------------- config ------------- */

  @Override
  public FlyerModel getFlyer(int i) {
    return flyers[i];
  }

  @Override
  public FlyerConfig[] getFlyerConfigs() {
    List<FlyerConfig> flyerConfigs = new ArrayList<FlyerConfig>();
    for (Flyer flyer: flyers) {
      flyerConfigs.add(flyer.getConfig());
    }
    return flyerConfigs.toArray(new FlyerConfig[0]);
  }

  @Override
  public List<FlyerModel> getFlyers() {
    return Arrays.asList(flyers);
  }
}
