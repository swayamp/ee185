package model;

import processing.core.PMatrix3D;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXModelBuilder;
import heronarts.lx.model.LXPoint;

public class FlyerModel extends LXModel implements Flyer {

  // Index of the flyer: its unique ID, starting from 0, that is used
  // to identify it and access array-based data structures.
  private int index;

  // Array storing references to the Flyer's two WingModels.
  private final WingModel[] wings;

  // The configuration state (
  private FlyerConfig config;

  private final LightSamplePointModel bodyLightPoint;

  // The offsets (distances) of the vertical bars on the front window
  // from the corner of the prow, in inches. Used to calculate the X
  // position of flyers.
  private final float[] VBAR_OFFSETS;
  // The offsets (distances) of the horizontal bars on the front window
  // from the ground, in inches. Used to calculate the Y position of
  // Fractal Flyers attached to the front window.
  private final float[] HBAR_OFFSETS;
  // The offsets (distances) of where the supporting pipes attach to the
  // back wall: these are at the same spacings as VBAR_OFFSETS but are
  // are a subset of them. BACK_WALL_OFFSETS[0] == VBAR_OFFSETS[0],
  // BACK_WALL_OFFSETS[1] == VBAR_OFFSETS[2], BACK_WALL_OFFSETS[2] ==
  // VBAR_OFFSETS[4], and BACK_WALL_OFFSETS[3] == VBAR_OFFSETS[6].
  private final float[] BACK_WALL_OFFSETS;


  // StairEdgePoint describes how the stairwell moves through
  // space.  It's used to place Flyers along the stairwell.
  // The fraction field describes how far along the stairwell this
  // point is; the first point is 0.0 and the final point is 1.0.
  // To place a flyer at position X along the stairwell, you find the
  // two points that straddle X and interpolate between them.
  private class StairEdgePoint {
    float fraction;
    float x;
    float y;
    float z;

    StairEdgePoint(float fraction, float x, float y, float z) {
      this.fraction = fraction;
      this.x = x;
      this.y = y;
      this.z = z;
    }

    StairEdgePoint(float fraction, PMatrix3D matrix) {
        this.fraction = fraction;

        float[] origin = {1, 1, 1};
        float[] newPos = new float[3];
        matrix.mult(origin, newPos);
        this.x = newPos[0];
        this.y = newPos[1];
        this.z = newPos[2];
    }
  }
  private final StairEdgePoint[] OUTER_STAIR_EDGE;
  private final StairEdgePoint[] INNER_STAIR_EDGE;


  public FlyerModel(FlyerConfig config) {
    this(new FlyerModelBuilder(config));
  }

  public FlyerModel(FlyerModelBuilder flyerModelBuilder) {
    super(flyerModelBuilder, false);

    this.index = flyerModelBuilder.config.index;

    this.config = flyerModelBuilder.config;

    this.wings = flyerModelBuilder.wings;
    this.bodyLightPoint = flyerModelBuilder.bodyLightPoint;

    // REVIEW(mcslee): should these be CONSTANT_CASE if they are memberFields?
    VBAR_OFFSETS = new float[Geometry.FRONT_WINDOW_VBAR_SPACINGS.length];
    float xPos = 0.0f;
    for (int i = 0; i < Geometry.FRONT_WINDOW_VBAR_SPACINGS.length; i++) {
        xPos += Geometry.FRONT_WINDOW_VBAR_SPACINGS[i];
        VBAR_OFFSETS[i] = xPos;
    }

    HBAR_OFFSETS = new float[Geometry.FRONT_WINDOW_HBAR_SPACINGS.length];
    float yPos = 0.0f;
    for (int i = 0; i < Geometry.FRONT_WINDOW_HBAR_SPACINGS.length; i++) {
        yPos += Geometry.FRONT_WINDOW_HBAR_SPACINGS[i];
        HBAR_OFFSETS[i] = yPos;
    }

    BACK_WALL_OFFSETS = new float[Geometry.BACK_WALL_SPACINGS.length];
    yPos = 0.0f;
    for (int i = 0; i < BACK_WALL_OFFSETS.length; i++) {
        yPos += Geometry.BACK_WALL_SPACINGS[i];
        BACK_WALL_OFFSETS[i] = yPos;
    }

    OUTER_STAIR_EDGE = new StairEdgePoint[16];
    PMatrix3D matrix = new PMatrix3D();
    // Move so center of bottom platform is at 0,0
    matrix.translate(FRONT_WINDOW_WIDTH * (1f + 0.707f) / 2f,
                 0f,
                 FRONT_WINDOW_WIDTH * (0.707f) / 2f);
    matrix.rotateY(- 5.0f/8.0f * (float)Math.PI);

    // The segments of the outer edge of the stairs are respresented
    // as a series of X,Y,Z points, with a normalized (0-1) value of
    // how far along the stairway it is. For a point P along the edge,
    // you find which two points it is between then interpolate between
    // them. Stairways are 4 units long, the edges of the corner are
    // 2 unit long, and the platform is 1 unit long, for total of
    // 12 (1 + 4 + 2 + 2 + 4 + 1) + (1 + 4 + 2 + 2 + 4 + 1) = 28 units.
    
    // Move to back left corner of platform, then rotate to direction
    // of stair and move away from the stair by 2 feet: flyers don't
    // hang directly below the stair edge
    matrix.translate(-168.0f, 0, 32.0f);
    matrix.rotateY((float)Math.PI/8f);
    matrix.translate(-2.0f * Geometry.FEET, 0, 0);

    OUTER_STAIR_EDGE[0] = new StairEdgePoint(0f, matrix);
    // Front left corner of platform
    matrix.translate(0, 0, 126f);
    OUTER_STAIR_EDGE[1] = new StairEdgePoint(1f/28f, matrix);

    // Up flight of stairs
    matrix.translate(0, 90f, 186f);
    OUTER_STAIR_EDGE[2] = new StairEdgePoint(5f/28f, matrix);

    // Move to corner of point platform
    matrix.translate(0, 0, 128f);
    OUTER_STAIR_EDGE[3] = new StairEdgePoint(7f/28f - 0.000001f, matrix);

    // Turn corner of point platform, move to last point by moving to actual
    // corner, turning, then moving out by 2f again
    matrix.translate(2.0f * Geometry.FEET, 0, 0);
    matrix.rotateY(3f *(float) Math.PI / 4f);
    matrix.translate(-2.0f * Geometry.FEET, 0, 0);
    OUTER_STAIR_EDGE[4] = new StairEdgePoint(7f/28f, matrix);
    matrix.translate(0, 0, 128f);
    OUTER_STAIR_EDGE[5] = new StairEdgePoint(9f/28f, matrix);
    matrix.translate(0, 90f, 186f);
    OUTER_STAIR_EDGE[6] = new StairEdgePoint(13f/28f, matrix);

    // To back right corner of platform
    matrix.translate(0, 0, 126f);
    OUTER_STAIR_EDGE[7] = new StairEdgePoint(14f/28f -0.00001f , matrix);    

    matrix = new PMatrix3D();
    // Move so center of middle platform is at 0,0
    matrix.translate(FRONT_WINDOW_WIDTH * (1f + 0.707f) / 2f,
                 180f,
                 FRONT_WINDOW_WIDTH * (0.707f) / 2f);
    matrix.rotateY(- 5.0f/8.0f *(float) Math.PI);

    // Move to back left corner of platform
    matrix.translate(-168.0f, 0, 32.0f);
    matrix.rotateY((float)Math.PI/8f);
    matrix.translate(-2.0f * Geometry.FEET, 0, 0);
    OUTER_STAIR_EDGE[8] = new StairEdgePoint(14f/28f, matrix);
    // Front left corner of platform
    matrix.translate(0, 0, 126f);
    OUTER_STAIR_EDGE[9] = new StairEdgePoint(15f/28f, matrix);
    
    //  Up flight of stairs
    matrix.translate(0, 90f, 186f);
    OUTER_STAIR_EDGE[10] = new StairEdgePoint(19f/28f, matrix);

    // Move to corner of point platform
    matrix.translate(0, 0, 128f);
    OUTER_STAIR_EDGE[11] = new StairEdgePoint(21f/28f - 0.000001f, matrix);

    // Turn corner of point platform, move to last point by
    // moving to actual corner, rotating, the moving back out
    // 2 feet.
    matrix.translate(2.0f * Geometry.FEET, 0, 0);
    matrix.rotateY(3f * (float)Math.PI / 4f);
    matrix.translate(-2.0f * Geometry.FEET, 0, 0);
    OUTER_STAIR_EDGE[12] = new StairEdgePoint(21f/28f, matrix);
    matrix.translate(0, 0, 128f);
    OUTER_STAIR_EDGE[13] = new StairEdgePoint(23f/28f, matrix);

    // Up flight of stairs
    matrix.translate(0, 90f, 186f);
    OUTER_STAIR_EDGE[14] = new StairEdgePoint(27f/28f, matrix);

    matrix.translate(0, 0, 126f);
    OUTER_STAIR_EDGE[15] = new StairEdgePoint(28f/28f, matrix);
    
    INNER_STAIR_EDGE = new StairEdgePoint[10];
    matrix = new PMatrix3D();
    // Move so center of bottom platform is at 0,0
    matrix.translate(FRONT_WINDOW_WIDTH * (1f + 0.707f) / 2f,
                 0f,
                 FRONT_WINDOW_WIDTH * (0.707f) / 2f);
    matrix.rotateY(- 5.0f/8.0f * (float)Math.PI);

    // The segments of the inner edge of the stairs are respresented
    // as a series of X,Y,Z points, with a normalized (0-1) value of
    // how far along the stairway it is. For a point P along the edge,
    // you find which two points it is between then interpolate between
    // them. Stairways are 2 units long and the platform is  1 unit long,
    // for total of 10 (2 + 2 + 1 + 2 + 2 + 1) units.
    matrix.translate((float)(- 168.0
                             + 126.0  * Math.sin(Math.PI/8.0) 
                             + 69.0   * Math.sin(5.0 / 8.0  * Math.PI) 
                             - 21.0   * Math.sin(Math.PI/8.0)),
                     0,
                     (float)(32.0
                         + 126.0 * Math.cos(Math.PI/8.0) 
                          + 69.0 * Math.cos(5.0 / 8.0 * Math.PI) 
                          - 21.0 * Math.cos(Math.PI/8.0)));
    matrix.rotateY(1.0f/8.0f * (float)Math.PI);
    matrix.translate(0,
                     0,
                     21f);
    INNER_STAIR_EDGE[0] = new StairEdgePoint(0, matrix);

    matrix.translate(0,
                     90.0f,
                     186.0f);
    INNER_STAIR_EDGE[1] = new StairEdgePoint(2f/10f, matrix);

    matrix.rotateY((float)(3.0 * Math.PI / 4.0));
    matrix.translate(0,
                     90.0f,
                     186.0f);
    INNER_STAIR_EDGE[2] = new StairEdgePoint(4f/10f - .0001f, matrix);

    matrix.translate(0, 0, 21f);
    
    // Edge of platform 
    INNER_STAIR_EDGE[3] = new StairEdgePoint(4f/10f, matrix);
    matrix.rotateY(5f/8f * (float)Math.PI);
    matrix.translate(0, 0, 180f);
    INNER_STAIR_EDGE[4] = new StairEdgePoint(5f/10f - .0001f, matrix);

    matrix = new PMatrix3D();
    // Move so center of middle platform is at 0,0
    matrix.translate(FRONT_WINDOW_WIDTH * (1f + 0.707f) / 2f,
                 180f,
                 FRONT_WINDOW_WIDTH * (0.707f) / 2f);
    matrix.rotateY(- 5.0f/8.0f * (float)Math.PI);
    // Move so center is at inner edge of bottom of stair
    matrix.translate((float)(- 168.0
                             + 126.0  * Math.sin(Math.PI/8.0) 
                             + 69.0   * Math.sin(5.0 / 8.0  * Math.PI) 
                             - 21.0   * Math.sin(Math.PI/8.0)),
                     0,
                     (float)(32.0
                         + 126.0 * Math.cos(Math.PI/8.0) 
                          + 69.0 * Math.cos(5.0 / 8.0 * Math.PI) 
                          - 21.0 * Math.cos(Math.PI/8.0)));
    matrix.rotateY(1.0f/8.0f * (float)Math.PI);
    matrix.translate(0,
                     0,
                     21f);
    INNER_STAIR_EDGE[5] = new StairEdgePoint(5f/10f, matrix);

    matrix.translate(0,
                     90.0f,
                     186.0f);
    INNER_STAIR_EDGE[6] = new StairEdgePoint(7f/10f, matrix);

    matrix.rotateY((float)(3.0 * Math.PI / 4.0));
    matrix.translate(0,
                     90.0f,
                     186.0f);
    INNER_STAIR_EDGE[7] = new StairEdgePoint(9f/10f - .0001f, matrix);

    matrix.translate(0, 0, 21f);
    // Edge of platform 
    INNER_STAIR_EDGE[8] = new StairEdgePoint(9f/10f, matrix);
    matrix.rotateY(5f/8f * (float)Math.PI);
    matrix.translate(0, 0, 180f);
    INNER_STAIR_EDGE[9] = new StairEdgePoint(10f/10f, matrix);
	

  }


  private static class FlyerModelBuilder extends LXModelBuilder {
      private final FlyerConfig config;
      private final LightSamplePointModel bodyLightPoint;
      private final WingModel[] wings = new WingModel[2];

      FlyerModelBuilder(FlyerConfig config) {
          this.config = config;
          this.wings[0] = new WingModel(config.index, false);
          this.wings[1] = new WingModel(config.index, true);       // Add the sub points from the wings to this model
          for (WingModel wing : this.wings) {
              for (LXPoint p : wing.points) {
                  addPoint(p);         }
          }
          // Add our body light point
          this.bodyLightPoint = new LightSamplePointModel(LightSamplePoint.LightCorners.BODY, null, config.index);
          addPoint(this.bodyLightPoint);
      }
  }

  @Override
  public int getIndex() {
    return this.index;
  }

  @Override
  public float getX() {
      // Transitioning this to a cached value would be a great
      // improvement. It would require triggering recalculation whenever
      // the FlyerConfig changes. -pal
      FlyerConfig.FaceConfig fConfig = this.config.faceConfig;
      String face = fConfig.face;
      if (face.equals("F")) {
          return fConfig.x;
      } else if (face.equals("E")) {
          float position = fConfig.position;
          for (int i = 1; i < OUTER_STAIR_EDGE.length; i++) {
              if (position <= OUTER_STAIR_EDGE[i].fraction) {
                  float start = OUTER_STAIR_EDGE[i - 1].x;
                  float end = OUTER_STAIR_EDGE[i].x;
                  float startFrac = OUTER_STAIR_EDGE[i-1].fraction;
                  float endFrac = OUTER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  float x = (ratio * end +
                             (1f - ratio) * start);
                  return x;
              }
          }
          return 10f;
      } else if (face.equals("I")) {
          float position = fConfig.position;
          for (int i = 1; i < INNER_STAIR_EDGE.length; i++) {
              if (position <= INNER_STAIR_EDGE[i].fraction) {
                  float start = INNER_STAIR_EDGE[i - 1].x;
                  float end = INNER_STAIR_EDGE[i].x;
                  float startFrac = INNER_STAIR_EDGE[i-1].fraction;
                  float endFrac = INNER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  float x = (ratio * end +
                             (1f - ratio) * start);
                  return x;
              }
         }
      } else if (face.equals("I")) {
          return this.index;
      } else if (face.equals("C")) {
          int pipe = (int)fConfig.pipe;
          float startx = 0.0f;
          float endx = 0.0f;
          switch (pipe) {
          case 0:
              startx = VBAR_OFFSETS[0];
              endx = BACK_WALL_OFFSETS[0] * 0.707f;
              break;
          case 1:
              startx = VBAR_OFFSETS[2];
              endx = BACK_WALL_OFFSETS[1] * 0.707f;
              break;
          case 2:
              startx = VBAR_OFFSETS[2];
              endx = (BACK_WALL_OFFSETS[2] * 0.707f + VBAR_OFFSETS[4]) / 2.0f;
              break;
          case 3:
              startx = BACK_WALL_OFFSETS[1];
              endx = (BACK_WALL_OFFSETS[2] * 0.707f + VBAR_OFFSETS[4]) / 2.0f;
              break;
          case 4:
              startx = VBAR_OFFSETS[4];
              endx = BACK_WALL_OFFSETS[2] * 0.707f;
              break;
          case 5:
              startx = VBAR_OFFSETS[4];
              endx = (BACK_WALL_OFFSETS[3] * 0.707f + VBAR_OFFSETS[6]) / 2.0f;
              break;
          case 6:
              startx = BACK_WALL_OFFSETS[2];
              endx = (BACK_WALL_OFFSETS[3] * 0.707f + VBAR_OFFSETS[6]) / 2.0f;
              endx = 0;
              break;
          case 7:
              startx = VBAR_OFFSETS[6];
              endx = BACK_WALL_OFFSETS[3] * 0.707f;
              break;
          default:
              System.err.println("ERROR: Invalid pipe specified for Flyer " + index + ": " + pipe + " -- valid values are 0-7");
              return 0.0f;
          }
          return startx += (fConfig.mountPoint) * (endx - startx);
      }
      return 0.0f;
  }

  @Override
  public float getY() {
      // Transitioning this to a cached value would be a great
      // improvement. It would require triggering recalculation whenever
      // the FlyerConfig changes. -pal
      FlyerConfig.FaceConfig fConfig = this.config.faceConfig;
      float offset = 0;
      if (fConfig.face.equals("F")) {
          offset = HBAR_OFFSETS[fConfig.y - 1];
      } else if (fConfig.face.equals("C")) {
          offset =  Geometry.FRONT_WINDOW_NADIR_HEIGHT -  Geometry.CEILING_PIPE_RADIUS;
      }
      else if (fConfig.face.equals("E")) {
          float position = fConfig.position;
          for (int i = 1; i < OUTER_STAIR_EDGE.length; i++) {
              if (position <= OUTER_STAIR_EDGE[i].fraction) {
                  float start = OUTER_STAIR_EDGE[i - 1].y;
                  float end = OUTER_STAIR_EDGE[i].y;
                  float startFrac = OUTER_STAIR_EDGE[i-1].fraction;
                  float endFrac = OUTER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  offset = (ratio * end +
                           (1f - ratio) * start);
                  break;
              }
          }
      } else if (fConfig.face.equals("I")) {
          float position = fConfig.position;
          for (int i = 1; i < INNER_STAIR_EDGE.length; i++) {
              if (position <= INNER_STAIR_EDGE[i].fraction) {
                  float start = INNER_STAIR_EDGE[i - 1].y;
                  float end = INNER_STAIR_EDGE[i].y;
                  float startFrac = INNER_STAIR_EDGE[i-1].fraction;
                  float endFrac = INNER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  offset = (ratio * end +
                             (1f - ratio) * start);
                  break;
              }
         }
      }
      return  offset - fConfig.hangDistance;
  }

  @Override
  public float getZ() {
      // Transitioning this to a cached value would be a great
      // improvement. It would require triggering recalculation whenever
      // the FlyerConfig changes. -pal
      FlyerConfig.FaceConfig fConfig = this.config.faceConfig;
      if (fConfig.face.equals("F")) {
          return 2.0f * Geometry.FEET;
      } else if (fConfig.face.equals("C")) {
          int pipe = (int)fConfig.pipe;
          float startz = 2.0f * Geometry.FEET;
          float endz = 0.0f;
          switch (pipe) {
          case 0:
              endz = (float)(BACK_WALL_OFFSETS[0] * Math.sin(Math.PI/4f));
              break;
          case 1:
              endz = (float)(BACK_WALL_OFFSETS[1] * Math.sin(Math.PI/4f));
              break;
          case 2:
              endz = (BACK_WALL_OFFSETS[2] * 0.707f) / 2.0f;
              break;
          case 3:
              startz = (float)(BACK_WALL_OFFSETS[1] * Math.sin(Math.PI/4f));
              endz = (BACK_WALL_OFFSETS[2] * 0.707f) / 2.0f;
              break;
          case 4:
              endz = (float)(BACK_WALL_OFFSETS[2] * Math.sin(Math.PI/4f));
              break;
          case 5:
              endz = (BACK_WALL_OFFSETS[3] * 0.707f) / 2.0f;
              break;
          case 6:
              startz = (float)(BACK_WALL_OFFSETS[2] * Math.sin(Math.PI/4f));
              endz = (BACK_WALL_OFFSETS[3] * 0.707f) / 2.0f;
              break;
          case 7:
              endz =(float)(BACK_WALL_OFFSETS[3] * Math.sin(Math.PI/4f));
              break;
          default:
              System.err.println("ERROR: Invalid pipe specified for Flyer " + index + ": " + pipe + " -- valid values are 0-7");
              return 0.0f;
          }
          return startz += (fConfig.mountPoint) * (endz - startz);

      } else if (fConfig.face.equals("E")) {
          float position = fConfig.position;
          for (int i = 1; i < OUTER_STAIR_EDGE.length; i++) {
              if (position <= OUTER_STAIR_EDGE[i].fraction) {
                  float start = OUTER_STAIR_EDGE[i - 1].z;
                  float end = OUTER_STAIR_EDGE[i].z;
                  float startFrac = OUTER_STAIR_EDGE[i-1].fraction;
                  float endFrac = OUTER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  float z = (ratio * end +
                             (1f - ratio) * start);
                  return z;
              }
          }
          return 200f;
      } else if (fConfig.face.equals("I")) {
          float position = fConfig.position;
          for (int i = 1; i < INNER_STAIR_EDGE.length; i++) {
              if (position <= INNER_STAIR_EDGE[i].fraction) {
                  float start = INNER_STAIR_EDGE[i - 1].z;
                  float end = INNER_STAIR_EDGE[i].z;
                  float startFrac = INNER_STAIR_EDGE[i-1].fraction;
                  float endFrac = INNER_STAIR_EDGE[i].fraction;
                  float gap = endFrac - startFrac;
                  float ratio = (position - startFrac ) / gap;
                  return (ratio * end + (1f - ratio) * start);
              }
         }
      } 
      return (float)getIndex();
  }

  @Override
  public float getRotation() {
    return this.config.rotation;
  }

  @Override
  public float getTilt() {
    return this.config.tilt;
  }

  @Override
  public WingModel[] getWings() {
    return wings;
  }

  @Override
  public WingModel getLeftWing() {
    return wings[0];
  }

  @Override
  public WingModel getRightWing() {
    return wings[1];
  }

  @Override
  public LightSamplePointModel getBodyLightPoint() {
    return bodyLightPoint;
  }

  public float getHangDistance() {
    return this.config.faceConfig.hangDistance;
  }

    /*  public List<LightSamplePoint> getLightPoints() {
      List<LightSamplePoint> points = new ArrayList<LightSamplePoint>();
      points.add((LightSamplePoint)this.bodyLightPoint);
      points.addAll(((WingModel)wings[0]).getLightPoints());
      points.addAll(((WingModel)wings[1]).getLightPoints());
      return points;
      }*/


  @Override
  public FlyerConfig getConfig() {
    FlyerConfig config = new FlyerConfig();
    config.index = getIndex();
    config.rotation = getRotation();
    config.tilt = getTilt();

    config.faceConfig = this.config.faceConfig;
    config.metadata = this.config.metadata;

    return config;
  }

  @Override
  public String toString() {
    String str = super.toString() + "\n";
    str += "Flyer: " + getIndex() + "\n";
    str += "WingL:" + getLeftWing() + "\n";
    str += "WingR:" + getRightWing() + "\n";
    return str;
  }
}
