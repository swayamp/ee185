package model;
public interface LightSamplePoint extends Geometry {
  enum LightCorners {
    HEAD(0, 24), // half short (.09) + half long (.27) + .03 = .39 , 23.4 LEDS
    SIDE(1, 21), // half short (.09) + half medium (.23) + .03 = .35, 21 LEDS
    TAIL(2, 31), // half medium (.23) + half long (.26) + .03 = .52,  31.2 LEDs

    BODY(6, 99);
    //  The long edge of a wing is 20.69 inches. (.53m)
    //  The medium edge is 18.04 edges.  (.46m)
    //  The short edge is 7.06 inches.   (.18m)
    //  corners are .03 m each
    public final int position;
    public final int numPixels;
    private LightCorners(int position, int numPixels) {
      this.position = position;
      this.numPixels = numPixels;
    }
  }

  Integer getWingIndex();
  int getBodyIndex();
  LightCorners getLocation();
  int getIndex();

//  /**
//   * Index of this lightPoint in color buffer, colors[lightPoint.getIndex()]
//   */
//  // TODO (achen) make this cleaner
//  default int getIndex() {
//    Integer wingIndex = getWingIndex();
//    if (wingIndex != null) {
//      return getBodyIndex() * NUM_LIGHT_POINTS_PER_FLYER +
//        (wingIndex % NUM_WINGS_PER_FLYER) * NUM_LIGHT_POINTS_PER_WING +
//        getLocation().position;
//    }
//    return getBodyIndex() * NUM_LIGHT_POINTS_PER_FLYER + getLocation().position;
//  }
}
