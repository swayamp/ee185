package model;
public interface Wing extends Geometry {

  int flyerIndex = 0;

  boolean isRight = false;

  int getFlyerIndex();
  boolean getIsRight();

  int getIndex();


  int getSkew();
  void setSkew(int skew);

}
