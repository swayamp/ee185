package model;

import heronarts.lx.model.LXModel;
import heronarts.lx.model.LXModelBuilder;
import heronarts.lx.model.LXPoint;

public class WingModel extends LXModel implements Wing {
  private int flyerIndex;
  private int index;
  private boolean isRight;

  /**
   * Skew about the mount point.
   * degrees the wing is down or up, range 0 to 180
   */
  protected int skew;

  protected final LightSamplePointModel[] lightPoints;

  public WingModel(int flyerIndex, boolean isRight) {
    this(new WingModelBuilder(flyerIndex, isRight));
  }

  public WingModel(WingModelBuilder wingModelBuilder) {
    super(wingModelBuilder, false);
    this.lightPoints = wingModelBuilder.lightPoints;
    this.flyerIndex = wingModelBuilder.flyerIndex;
    this.isRight = wingModelBuilder.isRight;
    this.index = wingModelBuilder.index;
    this.skew = 90;
  }

  private static class WingModelBuilder extends LXModelBuilder {

    private final int flyerIndex;
    private final int index;
    private final boolean isRight;
    private final LightSamplePointModel[] lightPoints = new LightSamplePointModel[NUM_LIGHT_POINTS_PER_WING];

    private WingModelBuilder(int flyerIndex, boolean isRight) {
      this.flyerIndex = flyerIndex;
      this.isRight = isRight;

      int wingIndex = flyerIndex * NUM_WINGS_PER_FLYER + (isRight ? 1 : 0);
      this.lightPoints[0] = new LightSamplePointModel(LightSamplePoint.LightCorners.HEAD, wingIndex, flyerIndex);
      this.lightPoints[1] = new LightSamplePointModel(LightSamplePoint.LightCorners.SIDE, wingIndex, flyerIndex);
      this.lightPoints[2] = new LightSamplePointModel(LightSamplePoint.LightCorners.TAIL, wingIndex, flyerIndex);
      this.index = wingIndex;
      // REVIEW(mcslee): you can set it later and update the model, but you might want to specify X/Y/Z position for these now
      // which will depend upon composition of the overall flyer position with the wing position as well
      // Add points to the model
      for (LXPoint p : lightPoints) {
        addPoint(p);
      }
    }
  }

  @Override
  public int getIndex() {
    return this.index;
  }

  @Override
  public int getFlyerIndex() {
    return this.flyerIndex;
  }

  @Override
  public boolean getIsRight() {
     return this.isRight;
  }

  @Override
  public void setSkew(int skew) {
    this.skew = skew;
  }

  @Override
  public int getSkew() {
    return this.skew;
  }

  @Override
  public String toString() {
      String str = super.toString() + ", ";
      str += "fi: " + getFlyerIndex() + ", ";
      str += "wi: " + getIndex() + ", ";
      str += "skew: " + getSkew();
      return str;
  }
}