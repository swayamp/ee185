#!/usr/bin/env bash 

echo "Updating to latest software."
git pull
echo "Building software."
cd ui; ./compile.sh
cd ..
open FlightGui/FlightGui.pde

